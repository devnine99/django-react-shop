const handlePhone = (phone) => {
	const p = phone.replace(/[^[0-9]/gi, '');
	const len = p.length;

	if (len < 4) {
		return p;
	} else if (len >= 4 && len < 8) {
		return `${p.substring(0, 3)}-${p.substring(3, 7)}`;
	} else {
		return `${p.substring(0, 3)}-${p.substring(3, 7)}-${p.substring(7, 11)}`;
	}
};

export default handlePhone;