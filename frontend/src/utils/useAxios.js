import { useEffect, useState } from 'react';
import api from '../api/api';

const useAxios = (data) => {
	const initialState = {
		data: null,
		error: null,
		isLoading: true,
	};
	const [state, setState] = useState(initialState);
	const [trigger, setTrigger] = useState(0);

	const refetch = () => {
		setState({
			...state,
			isLoading: true
		});
		setTrigger(Date.now());
	};

	useEffect(() => {
		api(data)
			.then(res => {
				setState({
					...state,
					data: res.data,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false
				});
			})
	}, [trigger]);

	return {...state, refetch}
};

export default useAxios;