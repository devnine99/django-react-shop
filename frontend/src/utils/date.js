import moment from "moment";

export const date = (d) => {
  const term = moment()-moment(d);
  if (term < 60000) {
    return '방금전'
  } else if (term < 3600000) {
    return `${parseInt(term / 60000)} 분전`;
  } else if (term < 86400000) {
    return `${parseInt(term / 3600000)} 시간전`;
  } else if (term < 2592000000) {
    return `${parseInt(term / 86400000)} 일전`;
  } else {
    return moment(d).format('YYYY년 MM월 DD일');
  }
};