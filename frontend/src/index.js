import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import GlobalProvider from './components/GlobalProvider/GlobalProvider';
import Theme from './components/Theme/Theme';
import Header from './components/Header/Header';
import SnackBar from './components/SnackBar/SnackBar';
import Footer from './components/Footer/Footer';
import Interceptor from './components/Interceptor/Interceptor';
import Routes from './components/Routes/Routes';

const App = () => {
	return (
		<GlobalProvider>
			<Theme>
				<SnackBar />
				<BrowserRouter>
					<Interceptor />
					<Header />
					<Routes />
					<Footer />
				</BrowserRouter>
			</Theme>
		</GlobalProvider>
	);
};


ReactDOM.render(<App />, document.getElementById('root'));
