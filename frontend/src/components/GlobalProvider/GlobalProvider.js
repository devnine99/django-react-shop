import React, { createContext, useEffect, useReducer, useState } from 'react';
import { informationApi } from '../../api/informationApi';
import api from '../../api/api';

// 인포메이션
let informationInitialState = {
	siteName: '',
	companyName: '',
	owner: '',
	email: '',
	tell: '',
	address: '',
	businessLicense: '',
	mailOrderLicense: '',
	bankSet: null,
	snsSet: null,
};

// 어카운트
let account = localStorage.getItem('account') || sessionStorage.getItem('account') || '';
const accountInitialState = account ? JSON.parse(account) : {};
const accountReducer = (state = accountInitialState, action) => {
	switch (action.type) {
		case 'LOGIN':
 			action.maintain ? localStorage.setItem('account', JSON.stringify(action.account)) : sessionStorage.setItem('account', JSON.stringify(action.account));
			return action.account;
		case 'LOGOUT':
			localStorage.removeItem('account') || sessionStorage.removeItem('account');
			return {};
		default:
			return state;
	}
};

// 스낵바
const snackbarInitialState = {
	open: false,
	message: '',
	severity: '',
	url: '',
};
const snackbarReducer = (state = snackbarInitialState, action) => {
	switch (action.type) {
		case 'OPEN':
			return {
				open: true,
				message: action.message,
				severity: action.severity,
				url: action.url,
			};
		case 'CLOSE':
			return {
				open: false,
				message: '',
				severity: '',
				url: '',
			};
		default:
			return state;
	}
};

// 카트
let cart = localStorage.getItem('cart');
if (cart === null) {
	cart = [];
	localStorage.setItem('cart', JSON.stringify(cart));
} else {
	cart = JSON.parse(cart);
}
const cartInitialState = cart;
const cartReducer = (state = cartInitialState, action) => {
	switch (action.type) {
		case 'ADD':
			// 스토리지의 카트를 가져와 있으면 업데이트, 없으면 추가 (스토리지와 스태이트 모두 업데이트)
			let removedOverlapState = state.filter(s => (
			  action.items.findIndex(items => items.id === s.id && items.optionCombinationId === s.optionCombinationId) === -1
			));
			let addedState = action.items.concat(removedOverlapState);
			localStorage.setItem('cart', JSON.stringify(addedState));
			return addedState;
		case 'REMOVE':
			// 스토리지의 카트를 가져와 삭제 (스토리지와 스테이트 모두 업데이트)
			let removedState = state.filter((s, i) => !action.indexes.includes(i));
			localStorage.setItem('cart', JSON.stringify(removedState));
			return removedState;
		default:
			return state;
	}
};

// 오더
let order = localStorage.getItem('order');
if (order === null) {
	order = [];
	localStorage.setItem('order', JSON.stringify(order));
} else {
	order = JSON.parse(order);
}
const orderInitialState = order;
const orderReducer = (state = orderInitialState, action) => {
	switch (action.type) {
		case 'ADD':
			localStorage.setItem('order', JSON.stringify(action.items));
			return action.items;
		case 'CLEAR':
			localStorage.removeItem('order');
			return [];
		default:
			return state;
	}
};


export const InformationStateContext = createContext();
export const AccountStateContext = createContext();
export const AccountDispatchContext = createContext();
export const SnackbarStateContext = createContext();
export const SnackbarDispatchContext = createContext();
export const CartStateContext = createContext();
export const CartDispatchContext = createContext();
export const OrderStateContext = createContext();
export const OrderDispatchContext = createContext();
export const TaskStateContext = createContext();
export const TaskSetStateContext = createContext();


const GlobalProvider = ({children}) => {
	const [informationState, setInformationState] = useState(informationInitialState);
	useEffect(() => {
		api(informationApi.detail())
			.then(res => {
				setInformationState(res.data);
			})
			.catch(err => {

			});
	}, []);
	const [accountState, accountDispatch] = useReducer(accountReducer, accountInitialState);
	const [snackbarState, snackbarDispatch] = useReducer(snackbarReducer, snackbarInitialState);
	const [cartState, cartDispatch] = useReducer(cartReducer, cartInitialState);
	const [orderState, orderDispatch] = useReducer(orderReducer, orderInitialState);
	const [task, setTask] = useState({
		id: '',
		connected: true,
	});

	return (
		<InformationStateContext.Provider value={informationState}>
			<AccountStateContext.Provider value={accountState}>
				<AccountDispatchContext.Provider value={accountDispatch}>
					<SnackbarStateContext.Provider value={snackbarState} >
						<SnackbarDispatchContext.Provider value={snackbarDispatch} >
							<CartStateContext.Provider value={cartState}>
								<CartDispatchContext.Provider value={cartDispatch}>
									<OrderStateContext.Provider value={orderState}>
										<OrderDispatchContext.Provider value={orderDispatch}>
											<TaskStateContext.Provider value={task}>
												<TaskSetStateContext.Provider value={setTask}>
													{children}
												</TaskSetStateContext.Provider>
											</TaskStateContext.Provider>
										</OrderDispatchContext.Provider>
									</OrderStateContext.Provider>
								</CartDispatchContext.Provider>
							</CartStateContext.Provider>
						</SnackbarDispatchContext.Provider>
					</SnackbarStateContext.Provider>
				</AccountDispatchContext.Provider>
			</AccountStateContext.Provider>
		</InformationStateContext.Provider>
	);
};

export default GlobalProvider;
