import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Grid, Toolbar } from '@material-ui/core';
import {
	AccountStateContext,
	CartStateContext,
} from '../GlobalProvider/GlobalProvider';
import Hidden from '@material-ui/core/Hidden';
import Button from '@material-ui/core/Button';
import FaceOutlinedIcon from '@material-ui/icons/FaceOutlined';
import MenuOutlinedIcon from '@material-ui/icons/MenuOutlined';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import Box from '@material-ui/core/Box';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Badge from '@material-ui/core/Badge';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const Header = () => {
	const accountState = useContext(AccountStateContext);
	const cartState = useContext(CartStateContext);
	const {token} = accountState;

	const [account, setAccount] = useState(null);
	const handleAccount = (e) => {
		setAccount(e.currentTarget);
	};
	const handleAccountClose = () => {
		setAccount(null);
	};

	const [open, setOpen] = React.useState(false);
  const toggleDrawer = () => {
    setOpen(!open);
  };

	return (
		<AppBar position="sticky" color="inherit">
			<Toolbar>
				<Grid container justify="space-between" alignItems="center">
					<Grid item>
						<Button component={Link} to="/" color="inherit" startIcon={<FaceOutlinedIcon />}>CELEB PICKS</Button>
						<Hidden xsDown>
							<Button component={Link} to="/celeb" color="inherit">셀럽</Button>
						</Hidden>
					</Grid>
					<Grid item>
						{/*<IconButton color="inherit" component={Link} to="/celeb">*/}
						{/*	<SearchIcon />*/}
						{/*</IconButton>*/}
						<IconButton color="inherit" component={Link} to="/cart">
							<Badge badgeContent={cartState.length} color="primary">
								<ShoppingCartOutlinedIcon />
							</Badge>
						</IconButton>
						<Hidden xsDown>
							<IconButton onClick={handleAccount} color="inherit">
								<Badge variant="dot" invisible={!token} color="primary" >
									<AccountCircleOutlinedIcon />
								</Badge>
							</IconButton>
							{token ? (
								<Menu
									anchorEl={account}
									keepMounted
									open={Boolean(account)}
									onClose={handleAccountClose}
								>
									{accountState && accountState.type === 'celeb' && (
										<MenuItem onClick={handleAccountClose} component={Link} to="/celeb/profile">샵관리</MenuItem>
									)}
									<MenuItem onClick={handleAccountClose} component={Link} to="/profile">프로필</MenuItem>
									<MenuItem onClick={handleAccountClose} component={Link} to="/logout">로그아웃</MenuItem>
								</Menu>
							) : (
								<Menu
									anchorEl={account}
									keepMounted
									open={Boolean(account)}
									onClose={handleAccountClose}
								>
									<MenuItem onClick={handleAccountClose} component={Link} to="/login">로그인</MenuItem>
									<MenuItem onClick={handleAccountClose} component={Link} to="/register">회원가입</MenuItem>
								</Menu>
							)}
						</Hidden>
						<Hidden smUp>
							<IconButton onClick={toggleDrawer} color="inherit" size="small">
								<MenuOutlinedIcon />
							</IconButton>
							<Drawer anchor="right" open={open} onClose={toggleDrawer}>
                <List>
                  <Box width={250}>
										<Grid container justify="space-around">
											{token ? (
												<>
													<Grid item>
														<ListItem>
															<Button component={Link} to="/profile" onClick={toggleDrawer}>프로필</Button>
														</ListItem>
													</Grid>
													<Grid item>
														<ListItem>
															<Button component={Link} to="/logout" onClick={toggleDrawer}>로그아웃</Button>
														</ListItem>
													</Grid>
												</>
											) : (
												<>
													<Grid item>
														<ListItem>
															<Button component={Link} to="/login" onClick={toggleDrawer}>로그인</Button>
														</ListItem>
													</Grid>
													<Grid item>
														<ListItem>
															<Button component={Link} to="/register" onClick={toggleDrawer}>회원가입</Button>
														</ListItem>
													</Grid>
												</>
											)}
										</Grid>
                    <Divider />
										<ListItem button component={Link} to="/celeb"onClick={toggleDrawer}>
											<ListItemText primary="셀럽" />
										</ListItem>
										<Divider />
										{accountState && accountState.type === 'celeb' && (
											<>
												<ListItem button component={Link} to="/seller" onClick={toggleDrawer}>
													<ListItemText primary="셀러" />
												</ListItem>
												<Divider />
											</>
										)}
                  </Box>
                </List>
            	</Drawer>
						</Hidden>
					</Grid>
				</Grid>
			</Toolbar>
		</AppBar>
	);
};

export default Header;
