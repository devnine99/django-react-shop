import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';

const Refetch = ({refetch}) => {
	return (
		<IconButton aria-label="delete" onClick={refetch}>
			<RefreshIcon />
		</IconButton>
	);
};

export default Refetch;
