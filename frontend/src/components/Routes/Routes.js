import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import routes from '../../routes';

const Routes = () => {
	return (
		<main>
			<Switch>
				{routes.map((route, index) => (
					<Route key={index} {...route} />
				))}
				<Redirect to="/" />
			</Switch>
		</main>
	);
};

export default Routes;
