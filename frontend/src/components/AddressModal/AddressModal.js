import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import api from '../../api/api';
import { addressApi } from '../../api/addressApi';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Chip from '@material-ui/core/Chip';
import Pagination from '@material-ui/lab/Pagination';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import Box from '@material-ui/core/Box';
import DialogActions from '@material-ui/core/DialogActions';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/styles';
import FormHelperText from '@material-ui/core/FormHelperText';


const AddressModal = ({open, setOpen, onComplete}) => {
	const initialState = {
		keyword: '',
		data: null,
		error: null,
		page: 1,
	};
	const [state, setState] = useState(initialState);
	const {keyword, data, error, page} = state;
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

	const handleChange = (e) => {
		setState({
			...state,
			[e.target.name]: e.target.value,
		})
	};

	const handleClose = () => {
		setOpen(false);
		setTimeout(() => setState(initialState), 500);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (keyword) {
			api(addressApi.list({keyword, page: 1}))
				.then(res => {
					setState({
						...state,
						data: res.data,
						error: null,
						page: 1,
					});
				})
				.catch(err => {
					setState({
						...state,
						data: null,
						error: err.response && err.response.data,
						page: 1,
					})
				});
		}
	};

	const handlePageChange = (e, page) => {
		setState({
			...state,
			page: page
		});
	};

	useEffect(() => {
		if (keyword) {
			api(addressApi.list({keyword, page}))
				.then(res => {
					setState({
						...state,
						data: res.data,
						error: null,
						page,
					});
				})
				.catch(err => {
					setState({
						...state,
						data: null,
						error: err.response && err.response.data,
						page,
					})
				});
		}
	}, [page]);

	const handleComplete = (e, id) => {
		let d = data.address.find(a => a.id === id);
		onComplete(d);
		handleClose();
	};

	return (
		<Dialog
			style={{height: '100%'}}
			open={open}
			onClose={() => handleClose()}
			maxWidth="sm"
			fullWidth
			fullScreen={fullScreen}
		>
			<DialogTitle>
				<Grid container justify="space-between" alignItems="center">
					<Typography variant="h5">주소 검색</Typography>
					<IconButton onClick={() => handleClose()}>
						<CloseIcon />
					</IconButton>
				</Grid>
					<form onSubmit={(e) => handleSubmit(e)}>
						<FormControl fullWidth margin="dense">
							<TextField
								type="text"
								name="keyword"
								label="주소"
								autoComplete="keyword"
								autoFocus
								value={keyword}
								required
								onChange={(e) => handleChange(e)}
								error={error && error.keyword}
								variant="outlined"
								size="small"
								InputProps={{
									endAdornment: (
										<InputAdornment position="end">
											<IconButton size="small" onClick={(e) => handleSubmit(e)}>
												<SearchIcon />
											</IconButton>
										</InputAdornment>
									),
								}}
							/>
							{error && error.keyword && error.keyword.map((message, index) => (
								<FormHelperText key={index} error>{message}</FormHelperText>
							))}
						</FormControl>
					</form>
			</DialogTitle>
			<DialogContent dividers>
				<Grid container spacing={1} style={{minHeight: '100%'}}>
					{data && data.address && data.address.length !== 0 ? (
						data.address.map(a => (
						<Grid item xs={12} key={a.id}>
							<Card>
								<CardActionArea onClick={(e) => handleComplete(e, a.id)}>
									<CardContent>
										<Typography>
											<Chip label="우편번호" size="small" component="span" /> {a.postcode}
										</Typography>
										<Typography>
											<Chip label="도로명" color="primary" size="small" component="span" /> {a.roadAddress}
										</Typography>
										<Typography>
											<Chip label="지번" color="primary" size="small" component="span" /> {a.jibunAddress}
										</Typography>
									</CardContent>
								</CardActionArea>
							</Card>
						</Grid>
						))
					) : (
						<Grid item xs={12}>
							<Box my={1}>
								<Chip label="Tip" color="primary" size="small" />
								<Typography variant="subtitle1">아래와 같은 조합으로 검색을 하시면 더욱 정확한 결과가 검색됩니다.</Typography>
							</Box>
							<Box my={1}>
								<Typography variant="body1">도로명 + 건물번호</Typography>
								<Typography variant="body2" color="primary">예) 판교역로 235, 제주 첨단로 242</Typography>
							</Box>
							<Box my={1}>
								<Typography variant="body1">지역명(동/리) + 번지</Typography>
								<Typography variant="body2" color="primary">예) 삼편동 681, 제주 영평동 2181</Typography>
							</Box>
							<Box my={1}>
								<Typography variant="body1">지역명(동/리) + 건물명(아파트명)</Typography>
								<Typography variant="body2" color="primary">예) 분당 주공, 연수동 주공3차</Typography>
							</Box>
							<Box my={1}>
								<Typography variant="body1">사서함명 + 번호</Typography>
								<Typography variant="body2" color="primary">예) 분당우체국사서함 1~100</Typography>
							</Box>
						</Grid>
					)}
				</Grid>
			</DialogContent>
			{data && data.total && (
				<DialogActions>
					<Grid container justify="center">
						<Pagination
							page={page}
							count={parseInt(data.total / 10) + (data.total % 10 ? 1 : 0)}
							color="primary"
							onChange={(e, page) => handlePageChange(e, page)}
						/>
					</Grid>
				</DialogActions>
			)}
		</Dialog>
	);
};

export default AddressModal;
