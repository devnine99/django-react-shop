import React from "react";
import { makeStyles, createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from '@material-ui/core/CssBaseline';
import blue from '@material-ui/core/colors/blue';
import pink from '@material-ui/core/colors/pink';

const useStyles = makeStyles({
  '@global': {
    body: {
      wordBreak: 'break-word',
      wordWrap: 'break-word',
    },
    '#root': {
      display: 'flex',
      flexDirection: 'column',
      minHeight: '100vh',
    },
    main: {
      flex: 1,
    },
  },
});

const globalTheme = createMuiTheme({
  // typography: {
  //   fontFamily: 'Noto Sans KR, sans-serif'
  // },
  palette: {
    type: 'light',
    primary: blue,
    secondary: pink,
  },
  overrides: {
    MuiCardMedia: {
      root: {
        height: 0,
        paddingTop: "100%",
      },
      media: {
        paddingTop: "66%",
      },
    },
    MuiBadge: {
      root: {
        padding: "0 2px"
      },
    }
  }
});

const Theme = ({children}) => {
  useStyles();

  return (
    <ThemeProvider theme={globalTheme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};

export default Theme;