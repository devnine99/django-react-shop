import React, { useContext } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { SnackbarDispatchContext, SnackbarStateContext } from '../GlobalProvider/GlobalProvider';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const SnackBar = () => {
	const snackbarState = useContext(SnackbarStateContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);

	const handleClose = (e, reason) => {
		if (reason === 'clickaway') {
      return;
    }
		snackbarDispatch({type: 'CLOSE'});
	};

	return (
		<Snackbar
      key={Date.now()}
      anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
      open={snackbarState.open}
      autoHideDuration={4000}
			onClose={handleClose}
			// message={snackbarState.message}
			// action={
			// 	<Button color="secondary" size="small">test</Button>
			// }
    >
      <Alert elevation={6} variant="filled" severity={snackbarState.severity}>
				{snackbarState.message}
      </Alert>
    </Snackbar>
	);
};

export default SnackBar;
