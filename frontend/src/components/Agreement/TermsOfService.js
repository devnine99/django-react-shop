import React, { useState } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { DialogActions } from '@material-ui/core';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import api from '../../api/api';
import { informationApi } from '../../api/informationApi';
import ReactHtmlParser from 'react-html-parser';

const TermsOfService = ({field, onChange, error}) => {
	const [open, setOpen] = useState(false);
	const initialState = {
		termsOfService: '',
	};
	const [state, setState] = useState(initialState);
	const {termsOfService} = state;

	const handleOpen = () => {
		setOpen(true);
		api(informationApi.termsOfService())
			.then(res => {
				setState({
					...state,
					termsOfService: res.data.termsOfService,
				})
			})
			.catch(err => {

			});
	};

	return (
		<FormControl margin="none" fullWidth error={error && error.isTermsOfService}>
			<Grid container justify="space-between" alignItems="center">
				<Grid item>
					<FormControlLabel
						control={
							<Checkbox
								type="checkbox"
								name="isTermsOfService"
								size="small"
								checked={field}
								onChange={onChange}
							/>
						}
						label={<Typography variant="subtitle2">서비스 이용약관에 동의합니다.</Typography>}
					/>
				</Grid>
				<Grid item>
					<Button size="small" onClick={handleOpen}>내용보기</Button>
					<Dialog
						open={open}
						onClose={() => setOpen(false)}
						maxWidth="sm"
						fullWidth
					>
						<DialogTitle>서비스 이용약관</DialogTitle>
						<DialogContent dividers>
							{ReactHtmlParser(termsOfService)}
						</DialogContent>
						<DialogActions>
							<Button color="primary" onClick={() => setOpen(false)}>닫기</Button>
						</DialogActions>
					</Dialog>
				</Grid>
			</Grid>
			{error && error.isTermsOfService && error.isTermsOfService.map((message, index) => (
				<FormHelperText key={index} error>{message}</FormHelperText>
			))}
		</FormControl>
	);
};

export default TermsOfService;
