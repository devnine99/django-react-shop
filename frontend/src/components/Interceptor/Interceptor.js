import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import api from '../../api/api';
import { AccountDispatchContext, AccountStateContext, SnackbarDispatchContext } from '../GlobalProvider/GlobalProvider';

const Interceptor = () => {
	const accountState = useContext(AccountStateContext);
	const accountDispatch = useContext(AccountDispatchContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);

	useEffect(() => {
		api.interceptors.request.use((config) => {
			let account = sessionStorage.getItem('account') || localStorage.getItem('account');
  		account = JSON.parse(account);
			if (account) {
				config.headers.Authorization = `Token ${account.token}`;
			}
			return config;
		});

		api.interceptors.response.use(
			(response) => response,
			(err) => {
				let account = sessionStorage.getItem('account') || localStorage.getItem('account');
  			account = JSON.parse(account);
				if (account && err.response.status === 401) {
					accountDispatch({type: 'LOGOUT'});
					snackbarDispatch({type: 'OPEN', message: '로그인 정보가 만료되어 로그아웃되었습니다.', severity: 'warning'});
				}
				return Promise.reject(err);
		});
	}, [accountState]);

	return <></>;
};

export default Interceptor;
