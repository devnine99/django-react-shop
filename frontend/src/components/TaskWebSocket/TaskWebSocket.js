import React, { useContext, useEffect } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import { AnalysisSetStateContext, GlobalStateContext } from '../GlobalProvider/GlobalProvider';
import { Button, notification } from 'antd';

const AnalysisWebsocket = ({location: {pathname, search}}) => {
		const globalState = useContext(GlobalStateContext);
		const analysisSetState = useContext(AnalysisSetStateContext);
		const history = useHistory();
		const handleAnalysis = (e, address, key) => {
				notification.close(key);
				history.push(`/analysis/?q=${address}`);
		};
		const handleMessage = (message) => {
				let m = JSON.parse(message.data);
				analysisSetState(m);
				if (pathname + search !== `/analysis/?q=${m.address}`) {
				let key = `open${Date.now()}`;
				notification.open({
						message: 'Analysis Complete',
						description: `요청하신 ${m.address}의 분석이 완료되었습니다.`,
						btn: <Button onClick={(e) => handleAnalysis(e, m.address, key)}>이동</Button>,
						key: key,
				});
			}
	};
	useEffect(() => {
			let url = '';
			if (globalState.user.key) url = `ws://192.168.1.27:8080/ws/analysis/?token=${globalState.user.key}`;
			else url = 'ws://192.168.1.27:8080/ws/analysis/';
			const ws = new WebSocket(url);
			ws.onmessage = (message) => handleMessage(message);
	}, [globalState]);
	return (
	  	<></>
	);
};
export default withRouter(AnalysisWebsocket);