import React, { useEffect } from 'react';
import ReactQuill, { Quill } from 'react-quill';
import { ImageUpload }  from 'quill-image-upload';
import 'react-quill/dist/quill.snow.css';

Quill.register('modules/imageUpload', ImageUpload);

const Editor = ({value, onChange}) => {
	const modules = {
    toolbar: [
      [{ 'header': [1, 2, 3, false] }],
      ['bold', 'italic', 'underline', 'strike'],
      ['link', 'image', 'video'],
    ],
  };

	const formats = [
    '헤더', 'font', 'size',
    'bold', 'italic', 'underline', 'strike',
    'list', 'bullet', 'indent',
    'link', 'image', 'video'
  ];

	return (
		<ReactQuill
			value={value}
			onChange={onChange}
			modules={modules}
			formats={formats}
		/>
	);
};

export default Editor;
