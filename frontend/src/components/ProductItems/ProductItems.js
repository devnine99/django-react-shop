import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import { Link } from 'react-router-dom';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import intcomma from '../../utils/intcomma';
import Grid from '@material-ui/core/Grid';

const ProductItems = ({to, mainThumbnail, sellerName, title, price, commissionSet}) => {
	return (
		<Grid item xs={6} sm={4} md={3} lg={2}>
			<Card style={{height: '100%'}}>
        <CardActionArea component={Link} to={to} style={{height: '100%'}}>
          <CardMedia image={mainThumbnail} />
          <CardContent>
						<Typography variant="subtitle2" color="textSecondary" gutterBottom={true}>{sellerName}</Typography>
            <Typography variant="subtitle2">{title}</Typography>
            <Typography variant="body2">{intcomma(price)}원</Typography>
						{commissionSet && (
							<Typography variant="body2">
								{commissionSet.find(commission => commission.level === 'B').amount}%
								{' ~ '}
								{commissionSet.find(commission => commission.level === 'SS').amount}%
							</Typography>
						)}
          </CardContent>
        </CardActionArea>
      </Card>
		</Grid>
	);
};

export default ProductItems;
