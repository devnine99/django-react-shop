import React, { useContext } from 'react';
import { Link as L } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { InformationStateContext } from '../GlobalProvider/GlobalProvider';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import Divider from '@material-ui/core/Divider';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/styles';

const Footer = () => {
	const informationState = useContext(InformationStateContext);
	const {siteName, companyName, owner, email, tell, address, businessLicense, mailOrderLicense, bankSet, snsSet} = informationState;

	const theme = useTheme();

	return (
		<Box component="footer" bgcolor="#fff">
			<Container>
				<Box my={4}>
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<Box mb={2}>
								<Typography variant="h4">CELEB PICKS</Typography>
							</Box>
						</Grid>
						<Grid item xs={12} md={4}>
							<Typography>셀럽의 픽! 인플루언서 커머스.</Typography>
							<Box my={1}>
								{snsSet && snsSet.sort((a, b) => (
									a.type < b.type ? -1 : a.type > b.type ? 1 : 0
								)).map(sns => {
									switch (sns.type) {
										case 'facebook':
											return (
												<IconButton
													key={sns.id}
													style={{color: "#385899"}}
													size="small"
													target="_blank"
													href={`https://facebook.com/${sns.path}`}
												>
													<FacebookIcon />
												</IconButton>
											);
										case 'instagram':
											return (
												<IconButton
													key={sns.id}
													style={{color: '#c13584'}}
													size="small"
													target="_blank"
													href={`https://instagram.com/${sns.path}`}
												>
													<InstagramIcon />
												</IconButton>
											);
										case 'youtube':
											return  (
												<IconButton
													key={sns.id}
													style={{color: '#ff0200'}}
													size="small"
													target="_blank"
													href={`https://youtube.com/channel/${sns.path}`}
												>
													<YouTubeIcon />
												</IconButton>
											);
										default:
											return false;
									}
								})}
							</Box>
						</Grid>
						<Grid item xs>
							<Typography variant="h6">CS CENTER</Typography>
							<Typography variant="h6">1544-1544</Typography>
							<Typography variant="body2">10:00 am - 07:00 pm</Typography>
						</Grid>
						<Grid item xs>
							<Typography variant="h6">ACCOUNT</Typography>
								{bankSet && bankSet.map(bank => (
									<Box key={bank.id} >
										<Typography variant="body2" component="span" noWrap>{bank.name} </Typography>
										<Typography variant="body2" component="span" noWrap>{bank.num} </Typography>
										<Typography variant="body2" component="span" noWrap>({bank.holder}) </Typography>
									</Box>
								))}
						</Grid>
						<Grid item xs="auto" sm>
							<Typography variant="h6">JOIN US</Typography>
							<Typography variant="body2">
								<Link color="inherit" component={L} to="/seller/register">셀러 가입</Link>
							</Typography>
							<Typography variant="body2">
								<Link color="inherit" component={L} to="/celeb/register">셀럽 가입</Link>
							</Typography>
						</Grid>
					</Grid>
				</Box>
				<Divider />
				<Box my={2}>
					<Grid container justify="space-between">
						<Grid item xs="auto">
							<Typography variant="caption" noWrap>회사명: {companyName}</Typography>
							{' | '}
							<Typography variant="caption" noWrap>대표: {owner}</Typography>
							{' | '}
							<Typography variant="caption" noWrap>소재지: {address}</Typography>
							{' | '}
							<Typography variant="caption" noWrap>이메일: {email}</Typography>
							{' | '}
							<Typography variant="caption" noWrap>연락처: {tell}</Typography>
							{' | '}
							<Typography variant="caption" noWrap>
								사업자등록번호: {businessLicense}
								<Link color="inherit" target="_blank" href="https://celeb-picks.com">[사업자정보확인]</Link>
							</Typography>
							{' | '}
							<Typography variant="caption" noWrap>통신판매업신고번호: {mailOrderLicense}</Typography>
						</Grid>
						<Grid item>
							<Typography variant="caption">
								<Link component={L} color="inherit" to={'/about-us'}>회사소개</Link>
							</Typography>
							{' | '}
							<Typography variant="caption">
								<Link component={L} color="inherit" to={'/user-guide'}>이용안내</Link>
							</Typography>
							{' | '}
							<Typography variant="caption">
								<Link component={L} color="inherit" to={'/privacy-policy'}>개인정보취급방침</Link>
							</Typography>
							{' | '}
							<Typography variant="caption">
								<Link component={L} color="inherit" to={'/terms-of-service'}>서비스이용약관</Link>
							</Typography>
						</Grid>
						<Grid item>
							<Typography variant="caption">
								Copyright &copy; {new Date().getFullYear()} {siteName}. All rights reserved.
							</Typography>
						</Grid>
					</Grid>
				</Box>
			</Container>
		</Box>
	);
};

export default Footer;
