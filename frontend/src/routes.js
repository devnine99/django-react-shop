import Home from './view/Home/Home';
import Login from './view/Account/Login';
import Register from './view/Account/Register';
import CelebList from './view/Celeb/CelebList';
import CelebDetail from './view/Celeb/CelebDetail';
import SellerList from './view/Seller/SellerList';
import SellerDetail from './view/Seller/SellerDetail';
import Cart from './view/Cart/Cart';
import ProductDetail from './view/Product/ProductDetail';
import Profile from './view/Account/Profile';
import PasswordReset from './view/Account/PasswordReset';
import PasswordResetConfirm from './view/Account/PasswordResetConfirm';
import Order from './view/Order/Order';
import OrderComplete from './view/Order/OrderComplete';
import ProductCreate from './view/Product/ProductCreate';
import Logout from './view/Account/Logout';
import AboutUs from './view/Information/AboutUs';
import UserGuide from './view/Information/UserGuide';
import TermsOfService from './view/Information/TermsOfService';
import PrivacyPolicy from './view/Information/PrivacyPolicy';
import Test from './view/Test/Test';
import EmailVerificationConfirm from './view/Account/EmailVerificationConfirm';
import CelebRegister from './view/Celeb/CelebRegister';
import SellerRegister from './view/Seller/SellerRegister';
import CelebProfile from './view/Celeb/CelebProfile';
import CelebAdminShop from './view/Celeb/CelebAdminShop';
import CelebAdminProduct from './view/Celeb/CelebAdminProduct';
import CelebAdminOrder from './view/Celeb/CelebAdminOrder';

const routes = [
	{exact: true, path: '/', component: Home},
	{exact: true, path: '/login', component: Login},
	{exact: true, path: '/logout', component: Logout},
	{exact: true, path: '/register', component: Register},
	{exact: true, path: '/profile', component: Profile},
	{exact: true, path: '/celeb/profile', component: CelebProfile},
	{exact: true, path: '/admin/shop', component: CelebAdminShop},
	{exact: true, path: '/admin/product', component: CelebAdminProduct},
	{exact: true, path: '/admin/order', component: CelebAdminOrder},
	{exact: true, path: '/email/verification/confirm/:uid/:token', component: EmailVerificationConfirm},
	{exact: true, path: '/password/reset', component: PasswordReset},
	{exact: true, path: '/password/reset/confirm/:uid/:token', component: PasswordResetConfirm},
	{exact: true, path: '/celeb', component: CelebList},
	{exact: true, path: '/celeb/register', component: CelebRegister},
	{exact: true, path: '/@:celebName/product/:id', component: ProductDetail},
	{exact: false, path: '/@:celebName', component: CelebDetail},
	{exact: true, path: '/seller', component: SellerList},
	{exact: true, path: '/seller/register', component: SellerRegister},
	{exact: true, path: '/seller/@:sellerName', component: SellerDetail},
	{exact: true, path: '/seller/@:sellerName/product/:id', component: ProductDetail},
	{exact: true, path: '/product/create', component: ProductCreate},
	{exact: true, path: '/product/:id', component: ProductDetail},
	{exact: true, path: '/cart', component: Cart,},
	{exact: true, path: '/order', component: Order},
	{exact: true, path: '/order/complete', component: OrderComplete},
	{exact: true, path: '/about-us', component: AboutUs},
	{exact: true, path: '/user-guide', component: UserGuide},
	{exact: true, path: '/terms-of-service', component: TermsOfService},
	{exact: true, path: '/privacy-policy', component: PrivacyPolicy},
	{exact: true, path: '/test', component: Test},
];

export default routes;