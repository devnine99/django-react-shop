export const orderApi = {
	create: (form, items) => ({method: 'post', url: '/order/create/', data: {...form, items}}),
	complete: (merchantUid) => ({method: 'post', url: `/order/complete/`, data: {merchantUid}}),
};
