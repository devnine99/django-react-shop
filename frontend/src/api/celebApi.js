export const celebApi = {
  list: (keyword = '') => ({method: 'get', url:'/celeb/', params: {keyword}}),
  register: (data) => ({method: 'post', url: '/celeb/', data}),
  detail: (celebName) => ({method: 'get', url:`/celeb/${celebName}/`}),
  follow: (celebName) => ({method: 'patch', url: `/celeb/${celebName}/follow/`}),
  productList: (celebName) => ({method: 'get', url:`/celeb/${celebName}/product/`}),
  productDetail: (celebName, productId) => ({method: 'get', url: `/celeb/${celebName}/product/${productId}/`}),
  productCreate: (productId) => ({method: 'post', url: `/celeb/celeb_product/`, data: {product: productId}}),
  productDelete: (productId) => ({method: 'delete', url: `/celeb/celeb_product/${productId}/`}),
  productOrder: (productOrder) => ({method: 'patch', url: `/celeb/product/order/`, data: {productOrder}}),
  profile: () => ({method: 'get', url: '/celeb/profile/'}),
};