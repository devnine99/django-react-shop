import axios from 'axios';

const api = axios.create();
api.defaults.baseURL = 'http://127.0.0.1:8000/';
api.defaults.xsrfCookieName = 'csrftoken';
api.defaults.xsrfHeaderName = 'X-CSRFToken';

export default api;
