export const sellerApi = {
  list: () => ({method: 'get', url: '/seller/'}),
  register: (data) => ({method: 'post', url: '/seller/', data}),
  detail: (sellerName) => ({method: 'get', url: `/seller/${sellerName}/`}),
  productList: (sellerName) => ({method: 'get', url: `/seller/${sellerName}/product/`}),
  productDetail: (sellerName, productId) => ({method: 'get', url: `/seller/${sellerName}/product/${productId}/`}),
};