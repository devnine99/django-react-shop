export const productApi = {
  list: () => ({method: 'get', url: '/product/'}),
  cart: (ids) => ({method: 'get', url: '/product/cart/', params: {ids}}),
  create: (data) => ({method: 'post', url: '/product/', data: data}),
  detail: (id, celebName) => ({method: 'get', url: `/product/${id}/`, params: {celebName}}),
  update: (id, data) => ({method: 'patch', url: `/product/${id}/`, data: data}),
};
