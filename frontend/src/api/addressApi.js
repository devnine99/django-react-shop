export const addressApi = {
  list: (params) => ({
    method: 'get',
    url: `/address/`,
    params
  }),
};
