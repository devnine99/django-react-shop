export const informationApi = {
  detail: () => ({method: 'get', url:'/information/'}),
  aboutUs: () => ({method: 'get', url:'/information/about-us/'}),
  userGuide: () => ({method: 'get', url:'/information/user-guide/'}),
  termsOfService: () => ({method: 'get', url:'/information/terms-of-service/'}),
  privacyPolicy: () => ({method: 'get', url:'/information/privacy-policy/'}),
};