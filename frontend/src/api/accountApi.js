export const accountApi = {
  login: (data) => ({
    method: 'post',
    url: '/account/login/',
    data
  }),
  register: (data) => ({
    method: 'post',
    url: '/account/register/',
    data
  }),
  logout: () => ({
    method: 'post',
    url: '/account/logout/'
  }),
  user: () => ({
    method: 'get',
    url: '/account/user/'
  }),
  profile: () => ({
    method: 'get',
    url: '/account/profile/'
  }),
  profileUpdate: (data) => ({
    method: 'patch',
    url: '/account/profile/',
    data
  }),
  emailVerification: (data) => ({
    method: 'put',
    url: '/account/email/verification/',
    data
  }),
  emailVerificationConfirm: (data) => ({
    method: 'put',
    url: '/account/email/verification/confirm/',
    data
  }),
  passwordReset: (data) => ({
    method: 'post',
    url: '/account/password/reset/',
    data
  }),
  passwordResetConfirm: (data) => ({
    method: 'put',
    url: '/account/password/reset/confirm/',
    data
  }),
  follow: (celebId) => ({
    method: 'patch',
    url: `/account/${celebId}/follow/`,
  }),
};
