import React, { useContext, useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { sellerApi } from '../../api/sellerApi';
import Loading from '../../components/Loading/Loading';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useAxios from '../../utils/useAxios';
import Box from '@material-ui/core/Box';
import Refetch from '../../components/Refetch/Refetch';
import api from '../../api/api';
import { AccountStateContext } from '../../components/GlobalProvider/GlobalProvider';

const SellerItem = ({seller}) => {
	const {username, coverImage, name} = seller;
	const accountState = useContext(AccountStateContext);

	if (accountState.type !== 'celeb') return <Redirect to="/" />;

	return (
		<Grid item xs={6} sm={4} md={3}>
      <Card>
        <CardActionArea component={Link} to={`/seller/@${name}`}>
          <CardMedia
            image={coverImage}
          />
          <CardContent>
            <Typography variant="h6">{name}</Typography>
            <Typography variant="subtitle1">@{username}</Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
	);
};

const SellerList = () => {
	const initialState = {
		sellerSet: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {sellerSet, error, isLoading} = state;
	useEffect(() => {
		api(sellerApi.list())
			.then(res => {
				setState({
					...state,
					sellerSet: res.data,
					error: null,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				});
			});
	}, []);

	return (
		<Container>
			<Box my={2}>
				<Grid container spacing={2}>
					{sellerSet && sellerSet.map(seller => (
						<SellerItem
							key={seller.id}
							seller={seller}
						/>
					))}
				</Grid>
			</Box>
		</Container>
	);
};

export default SellerList;
