import React, { useContext, useEffect, useState } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import PrivacyPolicy from '../../components/Agreement/PrivacyPolicy';
import TermsOfService from '../../components/Agreement/TermsOfService';
import CircularProgress from '@material-ui/core/CircularProgress';
import Link from '@material-ui/core/Link';
import FacebookIcon from '@material-ui/icons/Facebook';
import IconButton from '@material-ui/core/IconButton';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import api from '../../api/api';
import { sellerApi } from '../../api/sellerApi';
import { SnackbarDispatchContext } from '../../components/GlobalProvider/GlobalProvider';
import handlePhone from '../../utils/phone';

const SellerRegister = () => {
	const history = useHistory();
	const initialState = {
		form: {
			username: '',
			password1: '',
			password2: '',
			email: '',
			phone: '',
			name: '',
			isPrivacyPolicy: false,
			isTermsOfService: false,
		},
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {form, error, isLoading} = state;

	const snackbarDispatch = useContext(SnackbarDispatchContext);

	const handleSubmit = (e) => {
		e.preventDefault();
		setState({
			...state,
			isLoading: true,
		});
		api(sellerApi.register(form))
			.then(res => {
				setState(initialState);
				snackbarDispatch({type: 'OPEN', message: `${res.data.username}님의 회원가입을 축하합니다.`, severity: 'success'});
				// 리다이렉트
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				});
			})
	};

	const handleChange = (e) => {
		if (e.target.name === 'phone') {
			e.target.value = handlePhone(e.target.value);
		}
		setState({
			...state,
			form: {
				...state.form,
				[e.target.name]: e.target.type !== 'checkbox' ? e.target.value : e.target.checked
			}
		});
	};

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">셀러 가입 신청</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="username"
									label="사용자명"
									autoComplete="username"
									size="small"
									value={form.username}
									required
									autoFocus
									onChange={(e) => handleChange(e)}
									error={error && error.username}
									variant="outlined"
								/>
								{error && error.username && error.username.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="name"
									label="셀러명"
									size="small"
									value={form.name}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.name}
									variant="outlined"
								/>
								{error && error.name && error.name.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="phone"
									label="연락처"
									autoComplete="phone"
									size="small"
									value={form.phone}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.phone}
									variant="outlined"
								/>
								{error && error.phone && error.phone.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="email"
									name="email"
									label="이메일"
									autoComplete="email"
									size="small"
									value={form.email}
									required
									onChange={e => handleChange(e)}
									error={error && error.email}
									variant="outlined"
								/>
								{error && error.email && error.email.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password1"
									label="비밀번호"
									size="small"
									autoComplete="password"
									value={form.password1}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password1}
									variant="outlined"
								/>
								{error && error.password1 && error.password1.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password2"
									label="비밀번호 확인"
									size="small"
									autoComplete="password"
									value={form.password2}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password2}
									variant="outlined"
								/>
								{error && error.password2 && error.password2.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>

							<PrivacyPolicy field={form.isPrivacyPolicy} onChange={handleChange} error={error}/>
							<TermsOfService field={form.isTermsOfService} onChange={handleChange} error={error}/>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">회원가입</Button>
								}
							</FormControl>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default withRouter(SellerRegister);
