import React, { useContext, useEffect, useState } from 'react';
import { Link, Redirect, Route, withRouter } from 'react-router-dom';
import { sellerApi } from '../../api/sellerApi';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Loading from '../../components/Loading/Loading';
import useAxios from '../../utils/useAxios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';
import api from '../../api/api';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import intcomma from '../../utils/intcomma';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import ProductItems from '../../components/ProductItems/ProductItems';
import { AccountStateContext } from '../../components/GlobalProvider/GlobalProvider';

const ProductList = ({match}) => {
	const accountState = useContext(AccountStateContext);
	const {params: {sellerName}} = match;
	const initialState = {
		productSet: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {productSet, error, isLoading} = state;

	useEffect(() => {
		setState({
			...state,
			isLoading: true,
		});
		api(sellerApi.productList(sellerName))
			.then(res => {
				setState({
					...state,
					productSet: res.data,
					error: null,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				})
			});
	}, [match]);

	if (accountState.type !== 'celeb') return <Redirect to="/" />;

	return (
		<Grid container spacing={2}>
			{productSet && productSet.map(product => (
				<ProductItems
					key={product.id}
					to={`/seller/@${sellerName}/product/${product.id}`}
					mainThumbnail={product.mainThumbnail}
					sellerName={product.sellerName}
					title={product.title}
					price={product.price}
					commissionSet={product.commissionSet}
				/>
			))}
		</Grid>
	);
};

const Seller = ({state, setState, match: {path}}) => {
	const {seller} = state;
	return (
		<Box my={2}>
			<Box my={2}>
				<Card>
					<CardContent>
						<Grid container justify="center" alignItems="center" spacing={4}>
							<Grid item>
								<Avatar alt={seller.name} src={seller.profileImage} style={{width: 120, height: 120}} />
							</Grid>
							<Grid item>
								<Grid container direction="column" alignItems="center" spacing={2}>
									<Grid item>
										<Typography variant="h6">{seller.name}</Typography>
									</Grid>
									<Grid item>
										<Grid container spacing={4}>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														{/*<Typography variant="body1">{intcomma(seller.productCount)}</Typography>*/}
													</Grid>
													<Grid item>
														<Typography variant="body2">상품</Typography>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
					</CardContent>
				</Card>
			</Box>
			<Box my={2}>
				<Route exact path={`${path}`} component={ProductList} />
			</Box>
		</Box>
	);
};

const SellerDetail = ({match}) => {
	const {params: {sellerName}} = match;
	const initialState = {
		seller: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {seller, error, isLoading} = state;
	useEffect(() => {
		setState({
			...state,
			isLoading: true,
		});
		api(sellerApi.detail(sellerName))
			.then(res => {
				setState({
					...state,
					seller: res.data,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				});
			})
	}, [sellerName]);

	return (
		<Container>
			<Backdrop open={isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
			{seller && (
				<Seller state={state} setState={setState} match={match} />
			)}
		</Container>
	);
};

export default withRouter(SellerDetail);
