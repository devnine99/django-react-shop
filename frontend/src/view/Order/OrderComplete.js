import React, { useContext, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import queryString from "query-string";
import Container from '@material-ui/core/Container';
import useAxios from '../../utils/useAxios';
import { orderApi } from '../../api/orderApi';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {
	// CartDispatchContext,
	// CartStateContext,
	OrderDispatchContext
} from '../../components/GlobalProvider/GlobalProvider';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import CardMedia from '@material-ui/core/CardMedia';
import intcomma from '../../utils/intcomma';

const payMethod = {
	card: '카드',
	cash: '무통장입금',
	trans: '실시간계좌이체',
	vbank: '가상계좌',
	phone: '핸드폰소액결제',
	samsung: '삼성페이'
};

const OrderComplete = ({location: {search}}) => {
	// const cartState = useContext(CartStateContext);
	// const cartDispatch = useContext(CartDispatchContext);
	const orderDispatch = useContext(OrderDispatchContext);
	const {merchant_uid: merchantUid} = queryString.parse(search);

	const {data, error} = useAxios(orderApi.complete(merchantUid));

	useEffect(() => {
		orderDispatch({type: 'CLEAR'});
	}, [data]);

	return (
		<Container>
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">주문 완료</Typography>
						</Box>
						{data && (
							<>
								<Box mb={2}>
									<Box mb={1}>
										<Typography variant="h5">주문 상품</Typography>
									</Box>
									<Table>
										<TableHead>
											<TableRow>
												<TableCell align="left">상품</TableCell>
												<TableCell align="center">수량</TableCell>
												<TableCell align="center">가격</TableCell>
											</TableRow>
										</TableHead>
										<TableBody>
											{data.orderItemSet.map((o, i) => (
												<TableRow key={i}>
													<TableCell align="left">
														<Grid container alignItems="center" spacing={2}>
															<Grid item xs={12} sm={6}>
																<CardMedia image={o.mainThumbnailUrl} />
															</Grid>
															<Grid item xs={12} sm={6}>
																<Typography>{o.title}</Typography>
																<Typography>{o.option}</Typography>
															</Grid>
														</Grid>
													</TableCell>
													<TableCell align="center">{o.quantity}</TableCell>
													<TableCell align="center">{`${intcomma(o.price * o.quantity)}원`}</TableCell>
												</TableRow>
											))}
										</TableBody>
									</Table>
								</Box>
								<Box mb={2}>
									<Box mb={1}>
										<Typography variant="h5">주문번호</Typography>
									</Box>
									<Typography>{data.merchantUid}</Typography>
								</Box>
								<Box mb={2}>
									<Box mb={1}>
										<Typography variant="h5">주문자 정보</Typography>
									</Box>
									<Typography>이름: {data.orderName}</Typography>
									<Typography>연락처: {data.orderPhone}</Typography>
									<Typography>이메일: {data.orderEmail}</Typography>
									<Typography>주소: {data.orderAddress1} {data.orderAddress2}</Typography>
								</Box>
								<Box mb={2}>
									<Box mb={1}>
										<Typography variant="h5">배송지 정보</Typography>
									</Box>
									<Typography>이름: {data.shipName}</Typography>
									<Typography>연락처1: {data.shipPhone1}</Typography>
									<Typography>연락처2: {data.shipPhone2}</Typography>
									<Typography>주소: {data.shipAddress1} {data.shipAddress2}</Typography>
									<Typography>배송메모: {data.shipMemo}</Typography>
								</Box>
								<Box mb={2}>
									<Box mb={1}>
										<Typography variant="h5">결제 정보</Typography>
									</Box>
									<Typography>결제방법: {payMethod[data.payMethod]}</Typography>
									{data.payMethod === 'vbank' && (
										<>
											<Typography>결제정보: {data.vbank.name} {data.vbank.num} (예금주: {data.vbank.holder})</Typography>
											<Typography>입금기한: {data.vbank.date}</Typography>
										</>
									)}
									{data.payMethod === 'cash' &&error(
										<>
											<Typography>결제정보: 국민은행 701401-01-434070 (예금주: 셀럽픽스)</Typography>
										</>
									)}
								</Box>
							</>
						)}
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default withRouter(OrderComplete);
