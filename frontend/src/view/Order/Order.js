import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import AddressModal from '../../components/AddressModal/AddressModal';
import api from '../../api/api';
import { orderApi } from '../../api/orderApi';
import { OrderStateContext } from '../../components/GlobalProvider/GlobalProvider';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import intcomma from '../../utils/intcomma';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import moment from 'moment';
import handlePhone from '../../utils/phone';


const Order = () => {
	const history = useHistory();
	const orderState = useContext(OrderStateContext);
	const initialState = {
		orderName: '',
		orderPhone: '',
		orderEmail: '',
		orderPostcode: '',
		orderAddress1: '',
		orderAddress2: '',
		shipName: '',
		shipPhone1: '',
		shipPhone2: '',
		shipPostcode: '',
		shipAddress1: '',
		shipAddress2: '',
		shipMemo: '',
		payMethod: 'card',
	};
	const [form, setForm] = useState(initialState);
	const {
		orderName,
		orderPhone,
		orderEmail,
		orderPostcode,
		orderAddress1,
		orderAddress2,
		shipName,
		shipPhone1,
		shipPhone2,
		shipPostcode,
		shipAddress1,
		shipAddress2,
		shipMemo,
		payMethod,
	} = form;
	const [shipType, setShipType] = useState('order');
	const [addressType, setAddressType] = useState('order');
	const [error, setError] = useState({});
	const [open, setOpen] = useState(false);

	useEffect(() => {
		const s1 = document.createElement('script');
		const s2 = document.createElement('script');
		s1.src = 'https://code.jquery.com/jquery-1.12.4.min.js';
		s2.src = 'https://cdn.iamport.kr/js/iamport.payment-1.1.5.js';
		document.head.appendChild(s1);
		document.head.appendChild(s2);
	}, []);

	const handleChange = (e) => {
		if (e.target.name.includes('Phone')) {
			e.target.value = handlePhone(e.target.value);
		}
		if (shipType === 'order') {
			setForm({
				...form,
				[e.target.name]: e.target.value,
				[e.target.name.replace('order', 'ship').replace('Phone', 'Phone1')]: e.target.value,
			})
		} else {
			setForm({
				...form,
				[e.target.name]: e.target.value,
			});
		}
	};

	const handleOpen = (t) => {
		setAddressType(t);
		setOpen(true);
	};

	const orderAddress2Input = useRef();
	const shipAddress2Input = useRef();
	const handleComplete = (d) => {
		if (addressType === 'order') {
			if (shipType === 'order') {
				setForm({
					...form,
					orderPostcode: d.postcode,
					orderAddress1: d.roadAddress,
					shipPostcode: d.postcode,
					shipAddress1: d.roadAddress,
				});
			} else {
				setForm({
					...form,
					orderPostcode: d.postcode,
					orderAddress1: d.roadAddress,
				});
			}
			setTimeout(() => orderAddress2Input.current.focus());
		} else {
			setForm({
				...form,
				shipPostcode: d.postcode,
				shipAddress1: d.roadAddress,
			});
			setTimeout(() => shipAddress2Input.current.focus());
		}
	};

	const handleShipChange = (e) => {
		setShipType(e.target.value);
		if (e.target.value === 'ship') {
			setForm({
				...form,
				shipName: '',
				shipPhone1: '',
				shipPhone2: '',
				shipPostcode: '',
				shipAddress1: '',
				shipAddress2: '',
			});
		} else {
			setForm({
				...form,
				shipName: orderName,
				shipPhone1: orderPhone,
				shipPostcode: orderPostcode,
				shipAddress1: orderAddress1,
				shipAddress2: orderAddress2,
			});
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		let items = orderState.map(o => ({
			product: o.id,
			celeb: o.celeb,
			optionCombination: o.optionCombinationId,
			title: o.title,
			option: o.option,
			quantity: o.quantity,
		}));
		api(orderApi.create(form, items))
			.then(res => {
				if (payMethod === 'cash') {
					history.push(`/order/complete?merchant_uid=${res.data.merchantUid}`);
				} else {
					const data = {
						pg: 'html5_inicis',
						pay_method: payMethod,
						merchant_uid: res.data.merchantUid,
						name: getOrderName(),
						amount: getTotalPrice(),
						buyer_email: form.orderEmail,
						buyer_name: form.orderName,
						buyer_tel: form.orderPhone,
						buyer_addr: `${form.orderAddress1} ${form.orderAddress2}`,
						buyer_postcode: form.orderPostcode,
						m_redirect_url: `${window.location.origin}/order/complete`,
						vbank_due: moment().add('13', 'days').format('YYYYMMDD2359'),
					};

					const callback = (response) => {
						const {success, imp_uid, merchant_uid, error_msg} = response;
						if (success) {
							history.push(`/order/complete?imp_uid=${imp_uid}&merchant_uid=${merchant_uid}`);
						} else {
							console.log(error_msg);
						}
					};

					const {IMP} = window;
					IMP.init('imp58429040');
					IMP.request_pay(data, callback);
				}
			})
			.catch(err => {
				setError(err.response && err.response.data);
			});
	};

	const getTotalPrice = useCallback(() => {
		return orderState.reduce((total, o) => {
			total += o.price * o.quantity;
			return total;
		}, 0);
	}, [orderState]);

	const getOrderName = useCallback(() => {
		if (orderState === 1) {
			return orderState[0].title;
		} else {
			return `${orderState[0].title} 등 ${orderState.length}개 상품`;
		}
	}, [orderState]);

	return (
		<Container>
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">주문서 작성</Typography>
						</Box>
						<Box mb={2}>
							<Box mb={1}>
								<Typography variant="h5">주문 상품</Typography>
							</Box>
							<Table>
								<TableHead>
									<TableRow>
										<TableCell align="left">상품</TableCell>
										<TableCell align="center">수량</TableCell>
										<TableCell align="center">가격</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{orderState && orderState.length > 0 && orderState.map((o, i) => (
										<TableRow key={i}>
											<TableCell align="left">
												<Grid container alignItems="center" spacing={2}>
													<Grid item xs={12} sm={6}>
														<CardMedia image={o.mainThumbnailUrl} />
													</Grid>
													<Grid item xs={12} sm={6}>
														<Typography>{o.title}</Typography>
														<Typography>{o.option}</Typography>
													</Grid>
												</Grid>
											</TableCell>
											<TableCell align="center">{o.quantity}</TableCell>
											<TableCell align="center">{`${intcomma(o.price * o.quantity)}원`}</TableCell>
										</TableRow>
									))}
								</TableBody>
							</Table>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)} autoComplete="false">
							<Box mb={2}>
								<Box mb={1}>
									<Typography variant="h5">주문자 정보</Typography>
								</Box>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="orderName"
										label="이름"
										value={orderName}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.name}
										variant="outlined"
										size="small"
									/>
									{error && error.name && error.name.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="orderPhone"
										label="연락처"
										value={orderPhone}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.phone}
										variant="outlined"
										size="small"
									/>
									{error && error.phone && error.phone.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="email"
										name="orderEmail"
										label="이메일"
										value={orderEmail}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.phone}
										variant="outlined"
										size="small"
									/>
									{error && error.phone && error.phone.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense" onFocus={e => e.target.blur()}>
									<TextField
										type="text"
										name="orderPostcode"
										label="우편번호"
										value={orderPostcode}
										required
										onChange={(e) => handleChange(e)}
										onFocus={() => handleOpen('order')}
										error={error && error.postcode}
										variant="outlined"
										size="small"
										InputProps={{
											readOnly: true,
											endAdornment: (
												<InputAdornment position="end">
													<SearchIcon />
												</InputAdornment>
											),
										}}
									/>
									{error && error.postcode && error.postcode.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="orderAddress1"
										label="기본주소"
										value={orderAddress1}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.address1}
										variant="outlined"
										size="small"
										multiline
										inputProps={{
											readOnly: true
										}}
									/>
									{error && error.address1 && error.address1.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
									  inputRef={orderAddress2Input}
										type="text"
										name="orderAddress2"
										label="상세주소"
										value={orderAddress2}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.address2}
										variant="outlined"
										size="small"
									/>
									{error && error.address2 && error.address2.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
							</Box>
							<Box my={2}>
								<Box mb={1}>
									<Typography variant="h5">배송지 정보</Typography>
								</Box>
								<FormControl component="fieldset">
									<RadioGroup
										aria-label="배송지 정보"
										name="shipType"
										row
										onChange={(e) => handleShipChange(e)}
										defaultValue="order"
									>
										<FormControlLabel
											value="order"
											control={<Radio color="secondary" />}
											label="주문자 정보와 동일"
											labelPlacement="end"
										/>
										<FormControlLabel
											value="ship"
											control={<Radio color="secondary" />}
											label="새로운 배송지"
											labelPlacement="end"
										/>
									</RadioGroup>
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="shipName"
										label="이름"
										value={shipName}
										disabled={shipType === 'order'}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.name}
										variant="outlined"
										size="small"
									/>
									{error && error.name && error.name.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="shipPhone1"
										label="연락처1"
										value={shipPhone1}
										disabled={shipType === 'order'}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.phone}
										variant="outlined"
										size="small"
									/>
									{error && error.phone && error.phone.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="shipPhone2"
										label="연락처2"
										value={shipPhone2}
										onChange={(e) => handleChange(e)}
										error={error && error.phone}
										variant="outlined"
										size="small"
									/>
									{error && error.phone && error.phone.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense" onFocus={e => e.target.blur()}>
									<TextField
										type="text"
										name="shipPostcode"
										label="우편번호"
										value={shipPostcode}
										disabled={shipType === 'order'}
										required
										onChange={(e) => handleChange(e)}
										onFocus={() => shipType === 'ship' && handleOpen('ship')}
										error={error && error.postcode}
										variant="outlined"
										size="small"
										InputProps={{
											readOnly: true,
											endAdornment: (
												<InputAdornment position="end">
													<SearchIcon />
												</InputAdornment>
											),
										}}
									/>
									{error && error.postcode && error.postcode.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="shipAddress1"
										label="기본주소"
										value={shipAddress1}
										disabled={shipType === 'order'}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.address1}
										variant="outlined"
										size="small"
										multiline
										inputProps={{
											readOnly: true
										}}
									/>
									{error && error.address1 && error.address1.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										inputRef={shipAddress2Input}
										type="text"
										name="shipAddress2"
										label="상세주소"
										value={shipAddress2}
										disabled={shipType === 'order'}
										required
										onChange={(e) => handleChange(e)}
										error={error && error.address2}
										variant="outlined"
										size="small"
									/>
									{error && error.address2 && error.address2.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
								<FormControl fullWidth margin="dense">
									<TextField
										type="text"
										name="shipMemo"
										label="배송메모"
										value={shipMemo}
										onChange={(e) => handleChange(e)}
										error={error && error.address2}
										variant="outlined"
										size="small"
										multiline
										rows={3}
									/>
									{error && error.address2 && error.address2.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
							</Box>
							<Box my={2}>
								<Box mb={1}>
									<Typography variant="h5">결제 수단</Typography>
								</Box>
								<FormControl component="fieldset">
									<RadioGroup
										aria-label="결제방법"
										name="payMethod"
										value={payMethod}
										row
										onChange={(e) => handleChange(e)}
									>
										<FormControlLabel
											value="card"
											control={<Radio color="secondary" />}
											label="신용카드"
											labelPlacement="end"
										/>
										<FormControlLabel
											value="cash"
											control={<Radio color="secondary" />}
											label="무통장입금"
											labelPlacement="end"
										/>
										<FormControlLabel
											value="trans"
											control={<Radio color="secondary" />}
											label="실시간계좌이체"
											labelPlacement="end"
										/>
										<FormControlLabel
											value="vbank"
											control={<Radio color="secondary" />}
											label="가상계좌"
											labelPlacement="end"
										/>
										<FormControlLabel
											value="phone"
											control={<Radio color="secondary" />}
											label="핸드폰소액결제"
											labelPlacement="end"
										/>
										<FormControlLabel
											value="samsung"
											control={<Radio color="secondary" />}
											label="삼성페이"
											labelPlacement="end"
										/>
									</RadioGroup>
								</FormControl>
								<Typography variant="h5">
									총 결제금액: <Box color="secondary.main" component="span">{intcomma(getTotalPrice())}</Box>원
								</Typography>
								<FormControl fullWidth margin="dense">
									<Button type="submit" variant="contained" color="secondary">결제하기</Button>
								</FormControl>
							</Box>
						</form>
					</Box>
				</Paper>
			</Box>
			<AddressModal
				open={open}
				setOpen={setOpen}
				onComplete={(e) => handleComplete(e)}
			/>
		</Container>
	);
};

export default Order;
