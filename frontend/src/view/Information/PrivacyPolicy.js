import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import api from '../../api/api';
import { informationApi } from '../../api/informationApi';
import ReactHtmlParser from 'react-html-parser';

const PrivacyPolicy = () => {
	const initialState = {
		privacyPolicy: '',
	};
	const [state, setState] = useState(initialState);
	const {privacyPolicy} = state;

	useEffect(() => {
		api(informationApi.privacyPolicy())
			.then(res => {
				setState({
					...state,
					privacyPolicy: res.data.privacyPolicy
				})
			})
			.catch(err => {

			});
	}, []);

	return (
		<Container>
			<Box my={2}>
				<Typography variant="h4">개인정보취급방침</Typography>
				{ReactHtmlParser(privacyPolicy)}
			</Box>
		</Container>
	);
};

export default PrivacyPolicy;
