import React, { useEffect, useState } from 'react';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import api from '../../api/api';
import { informationApi } from '../../api/informationApi';
import ReactHtmlParser from 'react-html-parser';

const TermsOfService = () => {
	const initialState = {
		termsOfService: '',
	};
	const [state, setState] = useState(initialState);
	const {termsOfService} = state;

	useEffect(() => {
		api(informationApi.termsOfService())
			.then(res => {
				setState({
					...state,
					termsOfService: res.data.termsOfService
				})
			})
			.catch(err => {

			});
	}, []);

	return (
		<Container>
			<Box my={2}>
				<Typography variant="h4">서비스이용약관</Typography>
				{ReactHtmlParser(termsOfService)}
			</Box>
		</Container>
	);
};

export default TermsOfService;
