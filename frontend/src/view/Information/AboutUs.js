import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import api from '../../api/api';
import { informationApi } from '../../api/informationApi';
import ReactHtmlParser from 'react-html-parser';

const AboutUs = () => {
	const initialState = {
		aboutUs: '',
	};
	const [state, setState] = useState(initialState);
	const {aboutUs} = state;

	useEffect(() => {
		api(informationApi.aboutUs())
			.then(res => {
				setState({
					...state,
					aboutUs: res.data.aboutUs
				})
			})
			.catch(err => {

			});
	}, []);

	return (
		<Container>
			<Box my={2}>
				<Typography variant="h4">회사소개</Typography>
				{ReactHtmlParser(aboutUs)}
			</Box>
		</Container>
	);
};

export default AboutUs;
