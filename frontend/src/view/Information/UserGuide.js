import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import api from '../../api/api';
import { informationApi } from '../../api/informationApi';
import ReactHtmlParser from 'react-html-parser';

const UserGuide = () => {
	const initialState = {
		userGuide: '',
	};
	const [state, setState] = useState(initialState);
	const {userGuide} = state;

	useEffect(() => {
		api(informationApi.userGuide())
			.then(res => {
				setState({
					...state,
					userGuide: res.data.userGuide
				})
			})
			.catch(err => {

			});
	}, []);

	return (
		<Container>
			<Box my={2}>
				<Typography variant="h4">이용약관</Typography>
				{ReactHtmlParser(userGuide)}
			</Box>
		</Container>
	);
};

export default UserGuide;
