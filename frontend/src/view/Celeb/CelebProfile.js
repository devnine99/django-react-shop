import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import Container from '@material-ui/core/Container';
import api from '../../api/api';
import { celebApi } from '../../api/celebApi';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import IconButton from '@material-ui/core/IconButton';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import intcomma from '../../utils/intcomma';

const CelebProfile = () => {
	const initialState = {
		data: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {data: celeb, error, isLoading} = state;

	useEffect(() => {
		setState({
			...state,
			isLoading: true,
		});
		api(celebApi.profile())
			.then(res => {
				setState({
					data: res.data,
					error: null,
					isLoading: false,
				});
			})
	}, []);

	return (
		<Container>
			<Box my={2}>
				{celeb && (
						<Card>
							<CardContent>
								<Grid container justify="center" alignItems="center" spacing={4}>
									<Grid item>
										<Avatar alt={celeb.name} src={celeb.profileImage} style={{width: 120, height: 120}} />
									</Grid>
									<Grid item>
										<Grid container direction="column" alignItems="center" spacing={2}>
											<Grid item>
												<Typography variant="h6">{celeb.name}</Typography>
											</Grid>
											<Grid item>
												{celeb.snsSet && celeb.snsSet.sort((a, b) => (
													a.type < b.type ? -1 : a.type > b.type ? 1 : 0
												)).map(sns => {
													switch (sns.type) {
														case 'facebook':
															return (
																<IconButton
																	key={sns.id}
																	style={{color: "#385899"}}
																	size="small"
																	target="_blank"
																	href={`https://facebook.com/${sns.path}`}
																>
																	<FacebookIcon />
																</IconButton>
															);
														case 'instagram':
															return (
																<IconButton
																	key={sns.id}
																	style={{color: '#c13584'}}
																	size="small"
																	target="_blank"
																	href={`https://instagram.com/${sns.path}`}
																>
																	<InstagramIcon />
																</IconButton>
															);
														case 'youtube':
															return  (
																<IconButton
																	key={sns.id}
																	style={{color: '#ff0200'}}
																	size="small"
																	target="_blank"
																	href={`https://youtube.com/channel/${sns.path}`}
																>
																	<YouTubeIcon />
																</IconButton>
															);
														default:
															return '';
													}
												})}
											</Grid>
											<Grid item>
												<Grid container spacing={4}>
													<Grid item>
														<Grid container direction="column" alignItems="center">
															<Grid item>
																<Typography variant="body1">{intcomma(celeb.productCount)}</Typography>
															</Grid>
															<Grid item>
																<Typography variant="body2">픽</Typography>
															</Grid>
														</Grid>
													</Grid>
													<Grid item>
														<Grid container direction="column" alignItems="center">
															<Grid item>
																<Typography variant="body1">{intcomma(celeb.postCount)}</Typography>
															</Grid>
															<Grid item>
																<Typography variant="body2">게시물</Typography>
															</Grid>
														</Grid>
													</Grid>
													<Grid item>
														<Grid container direction="column" alignItems="center">
															<Grid item>
																<Typography variant="body1">{intcomma(celeb.followerCount)}</Typography>
															</Grid>
															<Grid item>
																<Typography variant="body2">팔로워</Typography>
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							</CardContent>
						</Card>
				)}
				<Box>
					<Link to="/admin/shop">상품 픽!</Link>
					<Link to="/admin/product">상품 관리</Link>
					<Link to="/admin/order">주문 관리</Link>
				</Box>
			</Box>
		</Container>
	);
};

export default CelebProfile;
