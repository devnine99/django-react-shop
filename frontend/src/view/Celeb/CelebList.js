import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { celebApi } from '../../api/celebApi';
import Loading from '../../components/Loading/Loading';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useAxios from '../../utils/useAxios';
import Box from '@material-ui/core/Box';
import intcomma from '../../utils/intcomma';
import Avatar from '@material-ui/core/Avatar';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import FormHelperText from '@material-ui/core/FormHelperText';
import api from '../../api/api';

const CelebItem = ({celeb}) => {
	const {username, coverImage, name, productSet} = celeb;

	return (
		<Box my={1}>
			<Card>
				<CardActionArea component={Link} to={`/@${name}`}>
					<CardContent>
						<Grid container justify="center" alignItems="center" spacing={4}>
							<Grid item>
								<Avatar alt={celeb.name} src={celeb.profileImage} style={{width: 120, height: 120}} />
							</Grid>
							<Grid item>
								<Grid container direction="column" alignItems="center" spacing={2}>
									<Grid item>
										<Typography variant="h6">{celeb.name}</Typography>
									</Grid>
									<Grid item>
										<Grid container spacing={4}>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														<Typography variant="body1">{intcomma(celeb.productCount)}</Typography>
													</Grid>
													<Grid item>
														<Typography variant="body2">픽</Typography>
													</Grid>
												</Grid>
											</Grid>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														<Typography variant="body1">{intcomma(celeb.postCount)}</Typography>
													</Grid>
													<Grid item>
														<Typography variant="body2">게시물</Typography>
													</Grid>
												</Grid>
											</Grid>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														<Typography variant="body1">{intcomma(celeb.followerCount)}</Typography>
													</Grid>
													<Grid item>
														<Typography variant="body2">팔로워</Typography>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
					</CardContent>
				</CardActionArea>
			</Card>
		</Box>
	);
};

const CelebList = () => {
	const initialState = {
		celebSet: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {celebSet, error, isLoading} = state;
	const [keyword, setKeyword] = useState('');

	useEffect(() => {
		setState({
			...state,
			isLoading: true,
		});
		api(celebApi.list(keyword))
			.then(res => {
				setState({
					...state,
					celebSet: res.data,
					error: null,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				});
			})
	}, [keyword]);

	// const handleSubmit = (e) => {
	// 	e.preventDefault();
	// 	api(celebApi.list(keyword))
	// 		.then(res => {
	// 			setState({
	// 				...state,
	// 				celebSet: res.data
	// 			});
	// 		})
	// 		.catch(err => {
	// 			setState({
	// 				...state,
	// 				error: err.response.data,
	// 			})
	// 		});
	// };

	const handleChange = (e) => {
		setKeyword(e.target.value);
	};

	return (
		<Container>
			<Box my={2}>
				<form>
					<FormControl fullWidth margin="dense">
						<TextField
							type="text"
							name="keyword"
							label="셀럽명"
							autoComplete="keyword"
							autoFocus
							value={keyword}
							onChange={(e) => handleChange(e)}
							error={error && error.keyword}
							variant="outlined"
							size="small"
							InputProps={{
								endAdornment: (
									<InputAdornment position="end">
										{/*<IconButton size="small" onClick={(e) => handleSubmit(e)}>*/}
											<SearchIcon />
										{/*</IconButton>*/}
									</InputAdornment>
								),
							}}
						/>
						{error && error.keyword && error.keyword.map((message, index) => (
							<FormHelperText key={index} error>{message}</FormHelperText>
						))}
					</FormControl>
				</form>
			</Box>
			<Box my={2}>
				{celebSet && celebSet.map(celeb => (
					<CelebItem
						key={celeb.id}
						celeb={celeb}
					/>
				))}
			</Box>
		</Container>
	);
};

export default CelebList;
