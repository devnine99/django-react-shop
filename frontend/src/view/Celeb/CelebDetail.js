import React, { useContext, useEffect, useState } from 'react';
import { Link, Route, withRouter } from 'react-router-dom';
import { celebApi } from '../../api/celebApi';
import { AccountStateContext, SnackbarDispatchContext } from '../../components/GlobalProvider/GlobalProvider';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import useAxios from '../../utils/useAxios';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import intcomma from '../../utils/intcomma';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import api from '../../api/api';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import IconButton from '@material-ui/core/IconButton';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import ProductItems from '../../components/ProductItems/ProductItems';
import { accountApi } from '../../api/accountApi';



const ProductList = ({match}) => {
	const {params: {celebName}} = match;
	const initialState = {
		productSet: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {productSet, error, isLoading} = state;
	useEffect(() => {
		setState({
			...state,
			isLoading: true,
		});
		api(celebApi.productList(celebName))
			.then(res => {
				setState({
					...state,
					productSet: res.data,
					error: null,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				});
			});
	}, []);

	return (
		<Grid container spacing={2}>
			{productSet && productSet.map(product => (
				<ProductItems
					key={product.id}
					to={`/@${celebName}/product/${product.id}`}
					mainThumbnail={product.mainThumbnail}
					sellerName={product.sellerName}
					title={product.title}
					price={product.price}
				/>
			))}
		</Grid>
	);
};

const PostList = () => {
	const celeb = {
		postSet: [
			{
				id: 1,
			}
		]
	};
	const {postSet} = celeb;

	return (
		<Grid container spacing={2}>
			{postSet && postSet.map(post => (
				<p key={post.id}>post</p>
			))}
		</Grid>
	);
};


const Celeb = ({state, setState, match: {path, url}}) => {
	const accountState = useContext(AccountStateContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const {celeb} = state;
	const [route, setRoute] = useState(`${url}`);

	const handleChange = (e, value) => {
		setRoute(value);
	};

	const handleFollow = () => {
		if (accountState.token) {
			api(accountApi.follow(celeb.id))
				.then(res => {
					setState({
						...state,
						celeb: {
							...celeb,
							followed: res.data.followed,
							followerCount: res.data.followed ? celeb.followerCount + 1 : celeb.followerCount - 1,
						}
					})
				})
				.catch(err => {

				});
		} else {
			snackbarDispatch({type: 'OPEN', message: '로그인 후 이용 가능합니다.', severity: 'error'});
		}
	};

	return (
		<Box my={2}>
			<Box my={2}>
				<Card>
					<CardContent>
						<Grid container justify="center" alignItems="center" spacing={4}>
							<Grid item>
								<Avatar alt={celeb.name} src={celeb.profileImage} style={{width: 120, height: 120}} />
							</Grid>
							<Grid item>
								<Grid container direction="column" alignItems="center" spacing={2}>
									<Grid item>
										<Typography variant="h6">{celeb.name}</Typography>
									</Grid>
									<Grid item>
										{celeb.followed ? (
											<Button
												variant="outlined"
												color="secondary"
												startIcon={<RemoveCircleOutlineOutlinedIcon />}
												onClick={handleFollow}
											>
												언팔로우
											</Button>
										) : (
											<Button
												variant="outlined"
												color="primary"
												startIcon={<AddCircleOutlineOutlinedIcon />}
												onClick={handleFollow}
											>
												팔로우
											</Button>
										)}
									</Grid>
									<Grid item>
										{celeb.snsSet && celeb.snsSet.sort((a, b) => (
											a.type < b.type ? -1 : a.type > b.type ? 1 : 0
										)).map(sns => {
											switch (sns.type) {
												case 'facebook':
													return (
														<IconButton
															key={sns.id}
															style={{color: "#385899"}}
															size="small"
															target="_blank"
															href={`https://facebook.com/${sns.path}`}
														>
															<FacebookIcon />
														</IconButton>
													);
												case 'instagram':
													return (
														<IconButton
															key={sns.id}
															style={{color: '#c13584'}}
															size="small"
															target="_blank"
															href={`https://instagram.com/${sns.path}`}
														>
															<InstagramIcon />
														</IconButton>
													);
												case 'youtube':
													return  (
														<IconButton
															key={sns.id}
															style={{color: '#ff0200'}}
															size="small"
															target="_blank"
															href={`https://youtube.com/channel/${sns.path}`}
														>
															<YouTubeIcon />
														</IconButton>
													);
												default:
													return '';
											}
										})}
									</Grid>
									<Grid item>
										<Grid container spacing={4}>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														<Typography variant="body1">{intcomma(celeb.productCount)}</Typography>
													</Grid>
													<Grid item>
														<Typography variant="body2">픽</Typography>
													</Grid>
												</Grid>
											</Grid>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														<Typography variant="body1">{intcomma(celeb.postCount)}</Typography>
													</Grid>
													<Grid item>
														<Typography variant="body2">게시물</Typography>
													</Grid>
												</Grid>
											</Grid>
											<Grid item>
												<Grid container direction="column" alignItems="center">
													<Grid item>
														<Typography variant="body1">{intcomma(celeb.followerCount)}</Typography>
													</Grid>
													<Grid item>
														<Typography variant="body2">팔로워</Typography>
													</Grid>
												</Grid>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
						</Grid>
					</CardContent>
				</Card>
			</Box>
			<Box my={2}>
				<Box my={2}>
					<Tabs
						value={route}
						onChange={handleChange}
						centered
					>
						<Tab label="상품" value={`${url}`} component={Link} to={`${url}`} />
						<Tab label="게시물" value={`${url}/post`} component={Link} to={`${url}/post`} />
					</Tabs>
				</Box>
				<Route exact path={`${path}`} component={ProductList} />
				<Route exact path={`${path}/post`} component={PostList} />
			</Box>
		</Box>
	);
};

const CelebDetail = ({match}) => {
	const {params: {celebName}} = match;
	const initialState = {
		celeb: null,
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {celeb, error, isLoading} = state;

	useEffect(() => {
		setState({
			...state,
			isLoading: true,
		});
		api(celebApi.detail(celebName))
			.then(res => {
				setState({
					...state,
					celeb: res.data,
					error: null,
					isLoading: false,
				});
			})
			.catch(err => {
				setState({
					...state,
					err: err.response && err.response.data,
					isLoading: false,
				});
			});
	}, [celebName]);

	return (
		<Container>
			<Backdrop open={isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
			{celeb && (
				<Celeb state={state} setState={setState} match={match} />
			)}
		</Container>
	);
};

export default withRouter(CelebDetail);
