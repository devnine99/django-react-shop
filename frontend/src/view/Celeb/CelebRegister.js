import React, { useContext, useEffect, useState } from 'react';
import { Link as L } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import PrivacyPolicy from '../../components/Agreement/PrivacyPolicy';
import TermsOfService from '../../components/Agreement/TermsOfService';
import CircularProgress from '@material-ui/core/CircularProgress';
import Link from '@material-ui/core/Link';
import FacebookIcon from '@material-ui/icons/Facebook';
import IconButton from '@material-ui/core/IconButton';
import InstagramIcon from '@material-ui/icons/Instagram';
import YouTubeIcon from '@material-ui/icons/YouTube';
import api from '../../api/api';
import { celebApi } from '../../api/celebApi';
import { SnackbarDispatchContext } from '../../components/GlobalProvider/GlobalProvider';
import handlePhone from '../../utils/phone';

const CelebRegister = () => {
	const initialState = {
		form: {
			username: '',
			password1: '',
			password2: '',
			email: '',
			phone: '',
			name: '',
			snsSet: [],
			isPrivacyPolicy: false,
			isTermsOfService: false,
		},
		error: null,
		isLoading: false,
	};
	const [state, setState] = useState(initialState);
	const {form, error, isLoading} = state;

	const snackbarDispatch = useContext(SnackbarDispatchContext);

	const handleSubmit = (e) => {
		e.preventDefault();
		setState({
			...state,
			isLoading: true,
		});
		api(celebApi.register(form))
			.then(res => {
				setState(initialState);
				snackbarDispatch({type: 'OPEN', message: `${res.data.username}님의 회원가입을 축하합니다.`, severity: 'success'});
				// 리다이렉트
			})
			.catch(err => {
				setState({
					...state,
					error: err.response && err.response.data,
					isLoading: false,
				});
			})
	};

	const handleChange = (e) => {
		if (e.target.name === 'phone') {
			e.target.value = handlePhone(e.target.value);
		}
		setState({
			...state,
			form: {
				...state.form,
				[e.target.name]: e.target.type !== 'checkbox' ? e.target.value : e.target.checked
			}
		});
	};

	const handleSNSPathChange = (e) => {
		setState({
			...state,
			form: {
				...state.form,
				snsSet: state.form.snsSet.map(sns => sns.type === e.target.name ? {...sns, path: e.target.value} : sns)
			}
		})
	};

	const handleSNS = (type) => {
		setState({
			...state,
			form: {
				...state.form,
				snsSet: state.form.snsSet.findIndex(sns => sns.type === type) === -1 ? [...state.form.snsSet, {type, path: ''}] : state.form.snsSet.filter(sns => sns.type !== type)
			}
		})
	};

	const getSNSName = (type) => {
		switch (type) {
			case 'F':
				return '페이스북';
			case 'I':
				return '인스타그램';
			case 'Y':
				return '유튜브';
			default:
				break;
		}
	};

	const getSNSPlaceholder = (type) => {
		switch (type) {
			case 'F':
				return '페이스북 아이디';
			case 'I':
				return '인스타그램 아이디';
			case 'Y':
				return '유튜브 URL PATH';
			default:
				break;
		}
	};

	const getSNSHelperText = (type) => {
		switch (type) {
			case 'F':
				return 'https://facebook.com/{페이스북 아이디}';
			case 'I':
				return 'https://instagram.com/{인스타그램 아이디}';
			case 'Y':
				return 'https://youtube.com/channel/{유튜브 URL PATH}';
			default:
				break;
		}
	};

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">셀럽 가입 신청</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="username"
									label="사용자명"
									autoComplete="username"
									size="small"
									value={form.username}
									required
									autoFocus
									onChange={(e) => handleChange(e)}
									error={error && error.username}
									variant="outlined"
								/>
								{error && error.username && error.username.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="name"
									label="셀럽명"
									size="small"
									value={form.name}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.name}
									variant="outlined"
								/>
								{error && error.name && error.name.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="phone"
									label="연락처"
									autoComplete="phone"
									size="small"
									value={form.phone}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.phone}
									variant="outlined"
								/>
								{error && error.phone && error.phone.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="email"
									name="email"
									label="이메일"
									autoComplete="email"
									size="small"
									value={form.email}
									required
									onChange={e => handleChange(e)}
									error={error && error.email}
									variant="outlined"
								/>
								{error && error.email && error.email.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password1"
									label="비밀번호"
									size="small"
									autoComplete="password"
									value={form.password1}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password1}
									variant="outlined"
								/>
								{error && error.password1 && error.password1.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password2"
									label="비밀번호 확인"
									size="small"
									autoComplete="password"
									value={form.password2}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password2}
									variant="outlined"
								/>
								{error && error.password2 && error.password2.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl margin="dense">
								<IconButton
									style={{color: form.snsSet.find(sns => sns.type === 'F') ? '#385899' : '#ccc'}}
									onClick={() => handleSNS('F')}
								>
									<FacebookIcon />
								</IconButton>
							</FormControl>
							<FormControl margin="dense">
								<IconButton
									style={{color: form.snsSet.find(sns => sns.type === 'I') ? '#c13584' : '#ccc'}}
									onClick={() => handleSNS('I')}
								>
									<InstagramIcon />
								</IconButton>
							</FormControl>
							<FormControl margin="dense">
								<IconButton
									style={{color: form.snsSet.find(sns => sns.type === 'Y') ? '#ff0200' : '#ccc'}}
									onClick={() => handleSNS('Y')}
								>
									<YouTubeIcon />
								</IconButton>
							</FormControl>
							{form.snsSet.sort((a, b) => (
								a.type < b.type ? -1 : a.type > b.type ? 1 : 0
							)).map(sns => (
								<FormControl key={sns.type} fullWidth margin="dense">
									<TextField
										type="text"
										name={sns.type}
										label={getSNSName(sns.type)}
										placeholder={getSNSPlaceholder(sns.type)}
										size="small"
										value={form.snsSet.find(s => s.type === sns.type).path}
										required
										onChange={(e) => handleSNSPathChange(e)}
										error={error && error.snsSet}
										variant="outlined"
									/>
									<FormHelperText>{getSNSHelperText(sns.type)}</FormHelperText>
									{error && error.snsSet && error.snsSet.map((message, index) => (
										<FormHelperText key={index} error>{message}</FormHelperText>
									))}
								</FormControl>
							))}

							<PrivacyPolicy field={form.isPrivacyPolicy} onChange={handleChange} error={error}/>
							<TermsOfService field={form.isTermsOfService} onChange={handleChange} error={error}/>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">회원가입</Button>
								}
							</FormControl>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default CelebRegister;
