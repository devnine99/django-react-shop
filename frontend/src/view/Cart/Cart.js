import React, { useContext, useEffect, useState } from 'react';
import { Link as L, useHistory } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import { Container } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import {
	CartDispatchContext,
	CartStateContext,
	OrderDispatchContext, SnackbarDispatchContext
} from '../../components/GlobalProvider/GlobalProvider';
import Button from '@material-ui/core/Button';
import { productApi } from '../../api/productApi';
import api from '../../api/api';
import TextField from '@material-ui/core/TextField';
import intcomma from '../../utils/intcomma';
import CardMedia from '@material-ui/core/CardMedia';

const Cart = () => {
	const history = useHistory();
	const cartState = useContext(CartStateContext);
	const cartDispatch = useContext(CartDispatchContext);
	const orderDispatch = useContext(OrderDispatchContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
  const [selected, setSelected] = useState([]);
	const [cartItems, setCartItems] = useState([]);

	useEffect(() => {
		let ids = cartState.reduce((acc, c) => {
			if (!acc.includes(c.id)) acc.push(c.id);
			return acc
		}, []).join(',');
		if (ids) {
			api(productApi.cart(ids))
				.then(res => {
					setCartItems(res.data);
				})
		}
	}, []);
	//
	// useEffect(() => {
	// 	cartItems.forEach(cartItem => {
	// 		getOptionCombinationStock(cartItem, )
	// 	})
	// }, [cartItems]);

  const handleSelectAllClick = (e) => {
    if (e.target.checked) {
      const newSelected = cartState.map((c, i)=> i);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const handleClick = (index) => {
    const selectedIndex = selected.indexOf(index);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, index);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
  };

  const handleSelectedDelete = () => {
  	cartDispatch({type: 'REMOVE', indexes: selected});
		setSelected([]);
	};

  // const handleDelete = (id, optionCombinationId) => {
  // 	let index = cartState.findIndex(c => c.id === id && c.optionCombinationId === optionCombinationId);
  // 	if (selected.includes(index)) {
  // 		let newSelected = selected;
  // 		newSelected.splice(newSelected.indexOf(index), 1);
  // 		setSelected(newSelected.map(s => s > index ? s - 1 : s));
	// 	}
  // 	cartDispatch({type: 'REMOVE', indexes: [index]});
	// };

  // 수량 변경
	const handleQuantity = (e, id, optionCombinationId) => {
		let items = cartState.map(c => (
			(c.id === id && c.optionCombinationId === optionCombinationId) ? {...c, quantity: parseInt(e.target.value)} : c
		));
		cartDispatch({type: 'ADD', items});
	};

	// 선택상품 주문
  const handleSelectedOrder = () => {
  	if (selected.length === 0) {
			snackbarDispatch({type: 'OPEN', message: '선택한 상품이 없습니다.', severity: 'warning'});
  		return
		}
		orderDispatch({type: 'ADD', items: cartState.filter((c, i) => selected.includes(i))});
  	history.push("/order");
	};

  // 전체상품 주문
  const handleAllOrder = () => {
		orderDispatch({type: 'ADD', items: cartState});
		history.push("/order");
	};

	const getTotalPrice = () => {
		return cartState.reduce((total, c, i) => {
			if (selected.includes(i)) {
				total += c.price * c.quantity;
			}
			return total;
		}, 0);
	};

	const getOptionCombination = (cartItem, optionCombinationId) => {
		return cartItem.optionCombinationSet.find(optionCombination => (
			optionCombination.id === optionCombinationId
		));
	};

	const getOptionCombinationPrice = (cartItem, optionCombinationId) => {
		let optionCombination = getOptionCombination(cartItem, optionCombinationId);
		let optionPrice = optionCombination.optionValueSet.reduce((acc, ov) => {
			acc += ov.price;
			return acc;
		}, 0);
		return cartItem.price + optionPrice;
	};

	const getOptionCombinationStock = (cartItem, optionCombinationId) => {
		if (!cartItem.isStock) return 999;
		let optionCombination = getOptionCombination(cartItem, optionCombinationId);
		return optionCombination.stock;
	};

  return (
    <Container>
			<Box my={2}>
				<Paper>
					<Toolbar>
						<Grid container justify="space-between" alignItems="center">
							<Grid item>
								{selected.length > 0 ? (
									<Typography color="inherit" variant="subtitle1">
										{selected.length}개 선택됨
									</Typography>
								) : (
									<Typography variant="h4" id="tableTitle">
										장바구니
									</Typography>
								)}
							</Grid>
							<Grid item>
								<Tooltip title="선택 삭제">
									<IconButton onClick={() => handleSelectedDelete()} aria-label="delete">
										<DeleteIcon />
									</IconButton>
								</Tooltip>
							</Grid>
						</Grid>
					</Toolbar>
					<TableContainer>
						<Table>
							<TableHead>
								<TableRow>
									<TableCell padding="checkbox">
										<Checkbox
											indeterminate={selected.length > 0 && selected.length < cartState.length}
											checked={cartState.length > 0 && selected.length === cartState.length}
											onChange={e => handleSelectAllClick(e)}
											inputProps={{ 'aria-label': 'select all desserts' }}
										/>
									</TableCell>
									<TableCell align="left">상품</TableCell>
									<TableCell align="center">수량</TableCell>
									<TableCell align="center">가격</TableCell>
									{/*<TableCell padding="checkbox" align="center">삭제</TableCell>*/}
								</TableRow>
							</TableHead>
							<TableBody>
								{cartItems.length > 0 && cartState && cartState.length > 0 ? (
									cartState.map((c, i) => {
										let cartItem = cartItems.find(cartItem => cartItem.id === c.id);
										let stock = getOptionCombinationStock(cartItem, c.optionCombinationId);
										let isActive = cartItem && stock;

										if (c.quantity > stock) {
											let items = cartState.map(cart => (
												(cart.id === c.id && cart.optionCombinationId === c.optionCombinationId) ? {...cart, quantity: stock} : cart
											));
											cartDispatch({type: 'ADD', items});
										}

										let price = getOptionCombinationPrice(cartItem, c.optionCombinationId);
										if (c.price !== price) {
											let items = cartState.map(cart => (
												(cart.id === c.id && cart.optionCombinationId === c.optionCombinationId) ? {...cart, price: price} : cart
											));
											cartDispatch({type: 'ADD', items});
										}

										return (
											<TableRow
												key={i}
												hover
												tabIndex={-1}
												aria-checked={selected.indexOf(i) !== -1}
												selected={selected.indexOf(i) !== -1}
											>
												<TableCell padding="checkbox">
													<Checkbox
														checked={selected.indexOf(i) !== -1}
														onClick={() => handleClick(i)}
													/>
												</TableCell>
												<TableCell align="left">
													<Grid container alignItems="center" spacing={2}>
														<Grid item xs={12} sm={6}>
															<CardMedia image={c.mainThumbnailUrl} />
														</Grid>
														<Grid item xs={12} sm={6}>
															<Typography style={{textDecoration: !isActive && 'line-through'}}>
																{isActive ? <Link color="inherit" to={`/product/${c.id}`} component={L}>{c.title}</Link> : c.title}
															</Typography>
															<Typography variant="body2" style={{textDecoration: !isActive && 'line-through'}}>{c.option}</Typography>
															{!isActive && <Typography>품절된 상품입니다.</Typography>}
														</Grid>
													</Grid>
												</TableCell>
												<TableCell align="center">
													<TextField
														inputProps={{min: "1", max: stock, step: "1"}}
														name={`${String(c.optionCombinationId)}`}
														type="number"
														label="수량"
														variant="outlined"
														size="small"
														value={c.quantity}
														disabled={!isActive}
														onChange={(e) => handleQuantity(e, c.id, c.optionCombinationId)}
													/>
												</TableCell>
												<TableCell align="center">
													<Typography style={{textDecoration: !isActive && 'line-through'}}>
														{`${intcomma(c.price * c.quantity)}원`}
													</Typography>
												</TableCell>
												{/*<TableCell padding="checkbox">*/}
												{/*	<Tooltip title="삭제">*/}
												{/*		<IconButton onClick={() => handleDelete(c.id, c.optionCombinationId)}>*/}
												{/*			<DeleteIcon />*/}
												{/*		</IconButton>*/}
												{/*	</Tooltip>*/}
												{/*</TableCell>*/}
											</TableRow>
										);
									})
								) : (
									<TableRow>
										<TableCell colSpan={4}>카트에 상품이 없습니다.</TableCell>
									</TableRow>
								)}
								<TableRow>
									<TableCell colSpan={4} align="right">
										<Typography variant="h5">
											총 상품금액: <Box color="secondary.main" component="span">{intcomma(getTotalPrice())}</Box>원
										</Typography>
									</TableCell>
								</TableRow>
							</TableBody>
						</Table>
					</TableContainer>
				</Paper>
			</Box>
			<Box my={3}>
				<Grid container justify="center" spacing={3}>
					<Grid item>
						<Button
							variant="contained"
							color="secondary"
							size="large"
							onClick={() => handleSelectedOrder()}
						>
							선택 상품 구매
						</Button>
					</Grid>
					<Grid item>
						<Button
							variant="contained"
							color="primary"
							size="large"
							onClick={() => handleAllOrder()}
						>
							전체 상품 구매
						</Button>
					</Grid>
				</Grid>
			</Box>
    </Container>
  );
};

export default Cart;
