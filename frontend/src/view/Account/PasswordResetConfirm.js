import React, { useContext, useReducer } from 'react';
import Container from '@material-ui/core/Container';
import api from '../../api/api';
import { accountApi } from '../../api/accountApi';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
	AccountStateContext,
	SnackbarDispatchContext
} from '../../components/GlobalProvider/GlobalProvider';
import { Redirect, withRouter } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const initialState = {
	form: {
		password1: '',
		password2: '',
	},
	error: null,
	isLoading: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE':
			return {
				...state,
				form: {
					...state.form,
					[action.target.name]: action.target.value
				}
			};
		case 'LOADING':
			return {
				...state,
				isLoading: true
			};
		case 'SUCCESS':
			return {
				...state,
				error: null,
				isLoading: false
			};
		case 'ERROR':
			return {
				...state,
				error: action.error,
				isLoading: false
			};
		default:
			return state;
	}
};

const PasswordResetConfirm = ({match: {params: {uid, token}}}) => {
	const accountState = useContext(AccountStateContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const [state, dispatch] = useReducer(reducer, initialState);
	const {form, error, isLoading} = state;

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch({type: 'LOADING'});
		api(accountApi.passwordResetConfirm({...form, uid, token}))
			.then(res => {
				dispatch({type: 'SUCCESS'});
				snackbarDispatch({type: 'OPEN', message: '비밀번호가 재설정되었습니다. 로그인 해주세요.', severity: 'success'});
			})
			.catch(err => {
				dispatch({type: 'ERROR', error: err.response && err.response.data});
				err.response && err.response.data.token && snackbarDispatch({type: 'OPEN', message: '이미 비밀번호를 변경했습니다.', severity: 'warning'});
			});
	};

	const handleChange = (e) => {
		dispatch({type: 'CHANGE', target: e.target});
	};

	if (accountState.key) return <Redirect to="/" />;

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">비밀번호 초기화</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password1"
									label="새로운 비밀번호"
									size="small"
									autoComplete="new-password"
									value={form.password1}
									required
									autoFocus
									onChange={(e) => handleChange(e)}
									error={error && error.password1}
									variant="outlined"
								/>
								{error && error.password1 && error.password1.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password2"
									label="새로운 비밀번호 확인"
									size="small"
									autoComplete="new-password"
									value={form.password2}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password2}
									variant="outlined"
								/>
								{error && error.password2 && error.password2.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">변경</Button>
								}
							</FormControl>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default withRouter(PasswordResetConfirm);
