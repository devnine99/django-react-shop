import React, { useContext, useReducer } from 'react';
import { Link as L, Redirect } from 'react-router-dom';
import {
	AccountDispatchContext,
	AccountStateContext,
	SnackbarDispatchContext,
} from '../../components/GlobalProvider/GlobalProvider';
import { accountApi } from '../../api/accountApi';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import api from '../../api/api';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';

const initialState = {
	form: {
		username: '',
		password: '',
		maintain: false,
	},
	error: null,
	isLoading: false,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE':
			if (action.target.name === 'maintain') {
				return {
					...state,
					form: {
						...state.form,
						[action.target.name]: action.target.checked
					}
				};
			} else {
				return {
					...state,
					form: {
						...state.form,
						[action.target.name]: action.target.value
					}
				};
			}
		case 'LOADING':
			return {
				...state,
				isLoading: true,
			};
		case 'SUCCESS':
			return {
				...state,
				error: null,
				isLoading: false
			};
		case 'ERROR':
			return {
				...state,
				error: action.error,
				isLoading: false,
			};
		default:
			return state;
	}
};

const Login = () => {
	const accountState = useContext(AccountStateContext);
	const accountDispatch = useContext(AccountDispatchContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const [state, dispatch] = useReducer(reducer, initialState);
	const {form, error, isLoading} = state;

	const handleChange = (e) => {
		dispatch({type: 'CHANGE', target: e.target})
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch({type: 'LOADING'});
		api(accountApi.login(form))
			.then(res => {
				dispatch({type: 'SUCCESS'});
				snackbarDispatch({type: 'OPEN', message: `${res.data.username}님 안녕하세요`, severity: 'success'});
				accountDispatch({type: 'LOGIN', maintain: form.maintain, account: res.data});
			})
			.catch(err => {
				dispatch({type: 'ERROR', error: err.response && err.response.data});
			});
	};

	if (accountState.token) return <Redirect to="/" />;

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">로그인</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="username"
									label="사용자명"
									autoComplete="username"
									size="small"
									value={form.username}
									required
									autoFocus
									onChange={(e) => handleChange(e)}
									error={error && error.username}
									variant="outlined"
								/>
								{error && error.username && error.username.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password"
									label="비밀번호"
									autoComplete="current-password"
									size="small"
									value={form.password}
									required
									onChange={e => handleChange(e)}
									error={error && error.password}
									variant="outlined"
								/>
								{error && error.password && error.password.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl>
								<FormControlLabel
									control={
										<Checkbox
											type="checkbox"
											name="maintain"
											size="small"
											checked={form.maintain}
											onChange={(e) => handleChange(e)}
										/>
									}
									label="로그인 상태 유지"
								/>
							</FormControl>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">로그인</Button>
								}
							</FormControl>
							<Box my={1}>
								<Grid container justify="space-between">
									<Grid item>
										<Link to="/password/reset" component={L}>비밀번호 찾기</Link>
									</Grid>
									<Grid item>
										<Link to="/register" component={L}>회원가입</Link>
									</Grid>
								</Grid>
							</Box>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default Login;
