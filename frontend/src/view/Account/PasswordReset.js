import React, { useContext, useReducer } from 'react';
import { Redirect } from 'react-router-dom';
import {
	AccountStateContext, SnackbarDispatchContext,
} from '../../components/GlobalProvider/GlobalProvider';
import { accountApi } from '../../api/accountApi';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import api from '../../api/api';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const initialState = {
	form: {
		email: ''
	},
	error: null,
	isLoading: false,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE':
			return {
				...state,
				form: {
					...state.form,
					[action.target.name]: action.target.value
				}
			};
		case 'LOADING':
			return {
				...state,
				isLoading: true,
			};
		case 'SUCCESS':
			return {
				...state,
				error: null,
				isLoading: false
			};
		case 'ERROR':
			return {
				...state,
				error: action.error,
				isLoading: false,
			};
		default:
			return state;
	}
};

const PasswordReset = () => {
	const accountState = useContext(AccountStateContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const [state, dispatch] = useReducer(reducer, initialState);
	const {form, error, isLoading} = state;

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch({type: 'LOADING'});
		api(accountApi.passwordReset(form))
			.then(res => {
				dispatch({type: 'SUCCESS'});
				snackbarDispatch({type: 'OPEN', message: '이메일 전송이 완료되었습니다.', severity: 'success'});
			})
			.catch(err => {
				dispatch({type: 'ERROR', error: err.response && err.response.data});
			});
	};

	const handleChange = (e) => {
		dispatch({type: 'CHANGE', target: e.target})
	};

	if (accountState.token) return <Redirect to="/" />;

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">비밀번호 초기화</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="email"
									label="이메일"
									autoComplete="email"
									size="small"
									value={form.email}
									required
									autoFocus
									onChange={(e) => handleChange(e)}
									error={error && error.email}
									variant="outlined"
								/>
								{error && error.email && error.email.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">이메일 전송</Button>
								}
							</FormControl>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default PasswordReset;
