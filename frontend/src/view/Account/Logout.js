import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import api from '../../api/api';
import { accountApi } from '../../api/accountApi';
import {
	AccountDispatchContext,
	AccountStateContext,
	SnackbarDispatchContext
} from '../../components/GlobalProvider/GlobalProvider';

const Logout = () => {
	const accountState = useContext(AccountStateContext);
	const accountDispatch = useContext(AccountDispatchContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);

	useEffect(() => {
		api(accountApi.logout())
			.then(res => {
				snackbarDispatch({type: 'OPEN', message: `${accountState.username}님 안녕히가세요.`, severity: 'success'});
				accountDispatch({type: 'LOGOUT'});
			}).catch(err => {
				snackbarDispatch({type: 'OPEN', message: `${accountState.username}님 안녕히가세요.`, severity: 'success'});
				accountDispatch({type: 'LOGOUT'});
		});
	}, []);

	if (!accountState.token) return <Redirect to="/" />;
	return <></>;
};

export default Logout;
