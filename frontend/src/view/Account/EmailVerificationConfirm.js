import React, { useEffect, useState } from 'react';
import api from '../../api/api';
import { accountApi } from '../../api/accountApi';
import { withRouter } from 'react-router-dom';
import Container from '@material-ui/core/Container';

const EmailVerificationConfirm = ({match: {params: {uid, token}}}) => {
	const [state, setState] = useState();
	useEffect(() => {
		api(accountApi.emailVerificationConfirm({uid, token}))
			.then(res => {
				setState({
					...state,
					data: res.data,
				})
			});
	}, []);

	return (
		<Container>
			완료
		</Container>
	);
};

export default withRouter(EmailVerificationConfirm);
