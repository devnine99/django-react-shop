import React, { useContext, useReducer } from 'react';
import { Redirect, Link as L, withRouter } from 'react-router-dom';
import queryString from 'query-string';
import Container from '@material-ui/core/Container';
import api from '../../api/api';
import { accountApi } from '../../api/accountApi';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
	AccountDispatchContext,
	AccountStateContext,
	SnackbarDispatchContext
} from '../../components/GlobalProvider/GlobalProvider';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import PrivacyPolicy from '../../components/Agreement/PrivacyPolicy';
import TermsOfService from '../../components/Agreement/TermsOfService';
import Paper from '@material-ui/core/Paper';
import handlePhone from '../../utils/phone';

const initialState = {
	form: {
		username: '',
		email: '',
		password1: '',
		password2: '',
		isPrivacyPolicy: false,
		isTermsOfService: false,
	},
	error: null,
	isLoading: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'CHANGE':
			return {
				...state,
				form: {
					...state.form,
					[action.target.name]: action.target.type !== 'checkbox' ? action.target.value : action.target.checked
				}
			};
		case 'LOADING':
			return {
				...state,
				isLoading: true
			};
		case 'SUCCESS':
			return {
				...state,
				error: null,
				isLoading: false
			};
		case 'ERROR':
			return {
				...state,
				error: action.error,
				isLoading: false
			};
		default:
			return state;
	}
};

const Register = ({location: {search}}) => {
	const parsed = queryString.parse(search);
	const accountState = useContext(AccountStateContext);
	const accountDispatch = useContext(AccountDispatchContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const [state, dispatch] = useReducer(reducer, initialState);
	const {form, error, isLoading} = state;

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch({type: 'LOADING'});
		api(accountApi.register(form))
			.then(res => {
				dispatch({type: 'SUCCESS'});
				snackbarDispatch({type: 'OPEN', message: `${res.data.username}님의 회원가입을 축하합니다.`, severity: 'success'});
				accountDispatch({type: 'LOGIN', maintain: false, account: res.data});
			})
			.catch(err => {
				dispatch({type: 'ERROR', error: err.response && err.response.data});
			});
	};

	const handleChange = (e) => {
		dispatch({type: 'CHANGE', target: e.target});
	};

	if (accountState.token) {
		if (parsed.next) return <Redirect to={parsed.next} />;
		else return <Redirect to="/" />;
	}

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">회원가입</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="username"
									label="사용자명"
									autoComplete="username"
									size="small"
									value={form.username}
									required
									autoFocus
									onChange={(e) => handleChange(e)}
									error={error && error.username}
									variant="outlined"
								/>
								{error && error.username && error.username.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="email"
									name="email"
									label="이메일"
									autoComplete="email"
									size="small"
									value={form.email}
									required
									onChange={e => handleChange(e)}
									error={error && error.email}
									variant="outlined"
								/>
								{error && error.email && error.email.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password1"
									label="비밀번호"
									size="small"
									autoComplete="password"
									value={form.password1}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password1}
									variant="outlined"
								/>
								{error && error.password1 && error.password1.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password2"
									label="비밀번호 확인"
									size="small"
									autoComplete="password"
									value={form.password2}
									required
									onChange={(e) => handleChange(e)}
									error={error && error.password2}
									variant="outlined"
								/>
								{error && error.password2 && error.password2.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<PrivacyPolicy field={form.isPrivacyPolicy} onChange={handleChange} error={error}/>
							<TermsOfService field={form.isTermsOfService} onChange={handleChange} error={error}/>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">회원가입</Button>
								}
							</FormControl>
							<Box my={1} textAlign="right">
								<Link to="/login" component={L}>로그인</Link>
							</Box>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default withRouter(Register);
