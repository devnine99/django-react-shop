import React, { useContext, useEffect, useReducer, useState } from 'react';
import { Redirect } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import FormHelperText from '@material-ui/core/FormHelperText';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import api from '../../api/api';
import { accountApi } from '../../api/accountApi';
import useAxios from '../../utils/useAxios';
import { AccountStateContext, SnackbarDispatchContext } from '../../components/GlobalProvider/GlobalProvider';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const initialState = {
	form: {
		username: '',
		email: '',
		password1: '',
		password2: '',
		isEmail: true,
	},
	error: null,
	isLoading: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'INIT':
			return {
				...state,
				form: {
					...state.form,
					...action.form,
				}
			};
		case 'CHANGE':
			return {
				...state,
				form: {
					...state.form,
					[action.target.name]: action.target.value,
				}
			};
		case 'LOADING':
			return {
				...state,
				isLoading: true,
			};
		case 'SUCCESS':
			return {
				...state,
				form: {
					...action.form,
					password1: '',
					password2: '',
				},
				error: null,
				isLoading: false,
			};
		case 'ERROR':
			return {
				...state,
				error: action.error,
				isLoading: false,
			};
		default:
			return state;
	}
};

const Profile = () => {
	const accountState = useContext(AccountStateContext);
	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const [state, dispatch] = useReducer(reducer, initialState);
	const {form, error, isLoading} = state;
	const [initEmail, setInitEmail] = useState();

	useEffect(() => {
		api(accountApi.profile())
			.then(res => {
				dispatch({type: 'INIT', form: res.data});
				setInitEmail(res.data.email);
			})
			.catch(err => {

			})
	}, []);

	const handleEmailVerification = (e) => {
		api(accountApi.emailVerification(form))
			.then(res => {
				dispatch({type: 'CHANGE', target: {name: 'isEmail', value: false}});
				snackbarDispatch({type: 'OPEN', message: '이메일 전송이 완료되었습니다.', severity: 'success'});
			});
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch({type: 'LOADING'});
		api(accountApi.profileUpdate(form))
			.then(res => {
				dispatch({type: 'SUCCESS', form: res.data});
				setInitEmail(form.email);
				snackbarDispatch({type: 'OPEN', message: '수정이 완료되었습니다.', severity: 'success'});
			})
			.catch(err => {
				dispatch({type: 'ERROR', error: err.response && err.response.data});
			});
	};

	const handleChange = (e) => {
		dispatch({type: 'CHANGE', target: e.target});
	};

	if (!accountState.token) return <Redirect to="/" />;

	return (
		<Container maxWidth="sm">
			<Box my={2}>
				<Paper>
					<Box p={2}>
						<Box mb={2}>
							<Typography variant="h4" align="center">프로필</Typography>
						</Box>
						<form onSubmit={(e) => handleSubmit(e)}>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="username"
									label="사용자명"
									autoComplete="username"
									size="small"
									value={form && form.username}
									required
									autoFocus
									onChange={e => handleChange(e)}
									error={error && error.username}
									variant="outlined"
									inputProps={{
										readOnly: true,
									}}
								/>
								{error && error.username && error.username.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="email"
									name="email"
									label="이메일"
									autoComplete="email"
									size="small"
									value={form && form.email}
									required
									onChange={e => handleChange(e)}
									error={error && error.email}
									variant="outlined"
								/>
								{!form.isEmail && (
									<FormHelperText>확인되지 않은 이메일입니다. 이메일 확인해주세요.</FormHelperText>
								)}
								{error && error.email && error.email.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							{!form.isEmail && (
								<FormControl fullWidth margin="dense">
									<Button
										type="button"
										variant="contained"
										color="primary"
										onClick={handleEmailVerification}
										disabled={initEmail !== form.email}
									>
										재전송
									</Button>
									{initEmail !== form.email && (<FormHelperText>메일 변경 후 자동으로 전송됩니다.</FormHelperText>)}
								</FormControl>
							)}
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password1"
									label="새로운 비밀번호"
									size="small"
									autoComplete="new-password"
									value={form && form.password1}
									onChange={e => handleChange(e)}
									error={error && error.password1}
									variant="outlined"
								/>
								<FormHelperText>비밀번호 변경 시에만 입력해 주세요.</FormHelperText>
								{error && error.password1 && error.password1.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="password"
									name="password2"
									label="새로운 비밀번호 확인"
									size="small"
									autoComplete="new-password"
									value={form && form.password2}
									onChange={e => handleChange(e)}
									error={error && error.password2}
									variant="outlined"
								/>
								<FormHelperText>비밀번호 변경 시에만 입력해 주세요.</FormHelperText>
								{error && error.password2 && error.password2.map((message, index) => (
									<FormHelperText key={index} error>{message}</FormHelperText>
								))}
							</FormControl>
							<FormControl fullWidth margin="dense">
								{isLoading
									?
										<Button type="submit" variant="contained" color="primary" disabled>
											<CircularProgress color="secondary" size={24} />
										</Button>
									:
										<Button type="submit" variant="contained" color="primary">수정</Button>
								}
							</FormControl>
						</form>
					</Box>
				</Paper>
			</Box>
		</Container>
	);
};

export default Profile;
