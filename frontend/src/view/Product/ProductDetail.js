import React, { createRef, useRef, useContext, useEffect, useState } from 'react';
import { withRouter, useHistory } from 'react-router-dom';
import {
	CartDispatchContext, CartStateContext,
	OrderDispatchContext,
	SnackbarDispatchContext
} from '../../components/GlobalProvider/GlobalProvider';
import { productApi } from '../../api/productApi';
import useAxios from '../../utils/useAxios';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import Divider from '@material-ui/core/Divider';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
import intcomma from '../../utils/intcomma';
import MobileStepper from '@material-ui/core/MobileStepper';
import { celebApi } from '../../api/celebApi';
import { sellerApi } from '../../api/sellerApi';
import api from '../../api/api';

const ProductDetail = ({match: {params: {id, celebName, sellerName}}}) => {
	let a;
	if (celebName) {
		a = celebApi.productDetail(celebName, id);
	} else {
		a = sellerApi.productDetail(sellerName, id);
	}
	const {data: product} = useAxios(a);

	const history = useHistory();

	const snackbarDispatch = useContext(SnackbarDispatchContext);
	const cartDispatch = useContext(CartDispatchContext);
	const orderDispatch = useContext(OrderDispatchContext);

	const [activeStep, setActiveStep] = useState(0);

	const [selectedOptionValues, setSelectedOptionValues] = useState([]);
	const [cartForms, setCartForms] = useState([]);

	const [labels, setLabels] = useState([]);
	const [labelWidths, setLabelWidths] = React.useState([]);

	useEffect(() => {
		// 옵션 상품이 아닐 때 초기 값 설정
		if (product && product.optionSet.length === 0) {
			let optionCombinationId = product.optionCombinationSet[0].id;
			setCartForms([{
				id: parseInt(id),
				celeb: product.celebId,
				mainThumbnail: product.mainThumbnail,
				title: product.title,
				price: getOptionCombinationPrice(optionCombinationId),
				optionCombinationId,
				quantity: 1,
			}]);
		}
		// 라벨 사이즈를 알기 위해 ref 생성
		product && product.optionSet.length > 0 && setLabels(product.optionSet.map(o => createRef()));
	}, [product]);

  React.useEffect(() => {
    setLabelWidths(labels.map(label => label.current.offsetWidth));
  }, [labels]);

	// 옵션 선택 했을 때
	const handleSelect = (e) => {
		// 이미 선택된 옵션일 때 수정, 선택되지 않은 옵션일 때 추가
		let selectedOptionIndex = selectedOptionValues.findIndex(selectedOptionValue => selectedOptionValue.optionName === e.target.name);
		if (selectedOptionIndex !== -1) {
			let newSelectedOptionValues = selectedOptionValues.map(selectedOptionValue => (
				selectedOptionValue.optionName === e.target.name ? {optionName: e.target.name, id: e.target.value} : selectedOptionValue
			));
			if (newSelectedOptionValues.length - 1 > selectedOptionIndex) {
				newSelectedOptionValues = newSelectedOptionValues.slice(0, selectedOptionIndex + 1);
			}
			setSelectedOptionValues(newSelectedOptionValues);
		} else {
			setSelectedOptionValues([...selectedOptionValues, {optionName: e.target.name, id: e.target.value}]);
		}
	};

	const getOptionCombinationId = () => {
		return product.optionCombinationSet.find(optionCombination => (
			optionCombination.optionValueSet.filter(optionValue => (
				selectedOptionValues.map(s => s.id).includes(optionValue.id)
			)).length === selectedOptionValues.length
		)).id;
	};

	useEffect(() => {
		// 옵션 모두 선택 했을 때 카트폼에 추가
		if (selectedOptionValues.length === (product && product.optionSet.length > 0 && product.optionSet.length)) {
			let optionCombinationId = getOptionCombinationId();
			// 이미 카트폼에 있는 경우
			if (cartForms.findIndex(cartForm => cartForm.optionCombinationId === optionCombinationId) !== -1) {
				snackbarDispatch({type: 'OPEN', message: '이미 선택된 옵션입니다.', severity: 'warning'});
			} else {
				setCartForms([
					...cartForms,
					{
						id: parseInt(id),
						celeb: product.celeb,
						mainThumbnail: product.mainThumbnail,
						title: product.title,
						option: getOption(optionCombinationId),
						price: getOptionCombinationPrice(optionCombinationId),
						optionCombinationId,
						quantity: 1
					}
				]);
				document.activeElement.blur();
			}
		}
	}, [selectedOptionValues]);

	// 카트폼이 업데이트 될 때 선택된 옵션 제거
	useEffect(() => {
		setSelectedOptionValues([]);
	}, [cartForms]);

	// 수량 변경
	const handleQuantity = (e) => {
		let newCartForms = cartForms.map(cartForm => (
			(cartForm.optionCombinationId === parseInt(e.target.name)) ? {...cartForm, quantity: parseInt(e.target.value)} : cartForm
		));
		setCartForms(newCartForms);
	};

	const handleDelete = (optionCombinationId) => {
		let newCartForms = cartForms.filter(cartForm => cartForm.optionCombinationId !== optionCombinationId);
		setCartForms(newCartForms);
	};

	const handleAddToCart = () => {
		if (cartForms.length !== 0) {
			if (celebName) {
				cartDispatch({type: 'ADD', items: cartForms});
			} else {
				cartDispatch({type: 'ADD', items: cartForms});
			}
			snackbarDispatch({type: 'OPEN', message: '카트에 상품을 담았습니다.', severity: 'success', url: '/cart'})
		} else {
			snackbarDispatch({type: 'OPEN', message: '옵션을 선택해주세요.', severity: 'warning'})
		}
	};

	const handleBuyItNow = () => {
		if (cartForms.length !== 0) {
			if (celebName) {
				cartDispatch({type: 'ADD', items: cartForms});
				orderDispatch({type: 'ADD', items: cartForms});
				history.push('/order');
			} else {
				cartDispatch({type: 'ADD', items: cartForms});
				orderDispatch({type: 'ADD', items: cartForms});
				history.push('/order');
			}
		} else {
			snackbarDispatch({type: 'OPEN', message: '옵션을 선택해주세요.', severity: 'warning'})
		}
	};

	const handleBuySample = () => {

	};

	const handleStartSell = () => {
		api(celebApi.productCreate(product.id))
			.then(res => {
				snackbarDispatch({type: 'OPEN', message: '성공적으로 추가되었습니다.', severity: 'success'})
			})
			.catch(err => {
				snackbarDispatch({type: 'OPEN', message: '이미 판매중인 상품입니다.', severity: 'warning'})
			});
	};

	const getOption = (optionCombinationId) => {
		let combination = product.optionCombinationSet.find(combination => combination.id === optionCombinationId);
		return combination.optionValueSet.reduce((acc, ov, i) => {
			let option = acc.concat(`${ov.optionName}: ${ov.name}`);
			if (i !== combination.optionValueSet.length - 1) {
				option = option.concat(' / ');
			}
			return option;
		}, '');
	};

	const getOptionValueSet = (option, optionIndex) => {
		return option.optionValueSet.filter(optionValue => {
			if (optionIndex > selectedOptionValues.length) return false;  // 이전 옵션이 선택되지 않으면 false
			else return product.optionCombinationSet.filter(optionCombination => {
				if (product.isStock && !optionCombination.stock) return false;  // 상품 재고 사용 and 재고 없을 경우 false
				else return selectedOptionValues.slice(0, optionIndex).filter(selectedOptionValue => (
						optionCombination.optionValueSet.findIndex(v => (
							v.id === selectedOptionValue.id
						)) !== -1
					)).length === selectedOptionValues.slice(0, optionIndex).length
				}).findIndex(optionCombination => (
					optionCombination.optionValueSet.filter(v => (
						v.id === optionValue.id
					)).length === 1
				)) !== -1;
		})
	};

	const getOptionCombination = (optionCombinationId) => {
		return product.optionCombinationSet.find(optionCombination => (
			optionCombination.id === optionCombinationId
		));
	};

	const getOptionCombinationPrice = (optionCombinationId) => {
		let optionCombination = getOptionCombination(optionCombinationId);
		let optionPrice = optionCombination.optionValueSet.reduce((acc, ov) => {
			acc += ov.price;
			return acc;
		}, 0);
		return product.price + optionPrice;
	};

	const getOptionCombinationStock = (optionCombinationId) => {
		if (!product.isStock) return 999;
		let optionCombination = getOptionCombination(optionCombinationId);
		return optionCombination.stock;
	};

	return (
		<Container>
			<Box my={2}>
				<Card>
					<Grid container>
						<Grid item xs={12} sm={6}>
							{/*<CardMedia*/}
							{/*	image={product && product.mainThumbnail}*/}
							{/*/>*/}
							<MobileStepper
								variant="dots"
								steps={product && product.thumbnailSet.length}
								position="static"
								activeStep={activeStep}
								// nextButton={
								// 	<Button size="small" onClick={handleNext} disabled={activeStep === 5}>
								// 		Next
								// 		{theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
								// 	</Button>
								// }
								// backButton={
								// 	<Button size="small" onClick={handleBack} disabled={activeStep === 0}>
								// 		{theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
								// 		Back
								// 	</Button>
								// }
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<CardContent>
								<Box my={1}>
									<Typography variant="subtitle1">{product && product.sellerName}</Typography>
									<Typography variant="h6">{product && product.title}</Typography>
									<Typography variant="body1">{product && intcomma(product.price)} 원</Typography>
									<Typography variant="body1">커미션</Typography>
									{sellerName && product && product.commissionSet.map(commission => (
										<Typography key={commission.id} variant="body2">{commission.level}등급: {commission.amount} %</Typography>
									))}
									<Divider />
								</Box>
								{product && product.optionSet.length > 0 && product.optionSet.map((option, optionIndex) => (
									<Box my={1} key={option.order}>
										<FormControl fullWidth variant="outlined" size="small">
											<InputLabel ref={labels && labels[optionIndex]}>{option.name}</InputLabel>
											<Select
												displayEmpty
												name={option.name}
												onChange={(e) => handleSelect(e)}
												disabled={optionIndex > selectedOptionValues.length || getOptionValueSet(option, optionIndex).length === 0}
												labelWidth={labelWidths && labelWidths[optionIndex]}
												value={(selectedOptionValues && selectedOptionValues.length - 1 >= optionIndex) ? selectedOptionValues[optionIndex].id : ''}
											>
												{getOptionValueSet(option, optionIndex).map(optionValue => (
													<MenuItem
														key={optionValue.id}
														value={optionValue.id}
													>
														<Grid container justify="space-between">
															<Grid item>
																{optionValue.name}
															</Grid>
															{optionValue.price !== 0 && (
																<Grid item>(+{optionValue.price}원)</Grid>
															)}
														</Grid>
													</MenuItem>
												))}
											</Select>
										</FormControl>
									</Box>
								))}
								{product && product.optionSet.length > 0 && product.optionSet.map((option, optionIndex) => (
									getOptionValueSet(option, optionIndex).length === 0 && '품절된 상품입니다.'
								))}
								{cartForms && cartForms.map((cartForm, i) => (
									<Box key={cartForm.optionCombinationId} my={2}>
										<Paper>
											<Box px={2} py={1}>
												<Grid container justify="space-between" alignItems="center">
													<Grid item>
														<Typography>{product.title}</Typography>
															{product && product.optionCombinationSet && product.optionCombinationSet.length > 0 && (
																<Typography variant="body2">{getOption(cartForm.optionCombinationId)}</Typography>
															)}
													</Grid>
													<Grid item>
														<Grid container alignItems="center">
															<Grid item>
																<TextField
																	inputProps={{min: "1", max: getOptionCombinationStock(cartForm.optionCombinationId), step: "1"}}
																	name={`${String(cartForm.optionCombinationId)}`}
																	type="number"
																	label="수량"
																	variant="outlined"
																	size="small"
																	value={cartForm.quantity}
																	onChange={(e) => handleQuantity(e)}
																/>
															</Grid>
															<Grid item>
																{product && product.optionSet.length > 0 && (
																	<Tooltip title="삭제">
																		<IconButton onClick={() => handleDelete(cartForm.optionCombinationId)} aria-label="delete">
																			<DeleteIcon />
																		</IconButton>
																	</Tooltip>
																)}
															</Grid>
															<Grid item>
																<Typography variant="body2">{intcomma(cartForm.price)}원</Typography>
															</Grid>
														</Grid>
													</Grid>
												</Grid>
											</Box>
										</Paper>
									</Box>
								))}
								{celebName ? (
									<Box>
										<Grid container spacing={1}>
											<Grid item xs={6}>
												<Button
													fullWidth
													type="button"
													variant="outlined"
													color="primary"
													onClick={handleAddToCart}
												>
													장바구니 담기
												</Button>
											</Grid>
											<Grid item xs={6}>
												<Button
													fullWidth
													type="button"
													variant="contained"
													color="primary"
													onClick={handleBuyItNow}
												>
													바로 구매하기
												</Button>
											</Grid>
										</Grid>
									</Box>
								) : (
									<Box>
										<Grid container spacing={1}>
											<Grid item xs={4}>
												<Button
													fullWidth
													type="button"
													variant="outlined"
													color="primary"
													onClick={handleAddToCart}
												>
													샘플신청
												</Button>
											</Grid>
											<Grid item xs={4}>
												<Button
													fullWidth
													type="button"
													variant="contained"
													color="primary"
													onClick={handleBuyItNow}
												>
													샘플구매
												</Button>
											</Grid>
											<Grid item xs={4}>
												<Button
													fullWidth
													type="button"
													variant="contained"
													color="secondary"
													onClick={handleStartSell}
												>
													판매시작
												</Button>
											</Grid>
										</Grid>
									</Box>
								)}
							</CardContent>
						</Grid>
					</Grid>
      	</Card>
			</Box>
		</Container>
	);
};

export default withRouter(ProductDetail);
