import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Editor from '../../components/Editor/Editor';

const ProductCreate = () => {
	const initialState = {
		title: '',
		content: '',
		price: '',
		isStock: false,
	};
	const [form, setForm] = useState(initialState);
	const {title, content, price, isStock} = form;

	const handleChange = e => {
		if (e.target.name === 'isStock') {
			setForm({
				...form,
				[e.target.name]: e.target.checked
			});
		} else {
			setForm({
				...form,
				[e.target.name]: e.target.value
			});
		}
	};

	const handleEditor = v => {
		setForm({
			...form,
			content: v,
		});
	};

	const handleSubmit = e => {
		e.preventDefault();

	};

	return (
		<Container>
			<Box my={2}>
				<Grid container>
					<Grid item xs={12}>
						<form
							autoComplete="off"
							onSubmit={(e) => handleSubmit(e)}
						>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="title"
									label="상품명"
									value={title}
									required
									onChange={handleChange}
									variant="outlined"
									size="small"
								/>
							</FormControl>
							<Editor
								value={content}
								onChange={handleEditor}
							/>
							<FormControl fullWidth margin="dense">
								<TextField
									type="text"
									name="content"
									label="상품설명"
									value={content}
									required
									onChange={handleChange}
									variant="outlined"
									size="small"
								/>
							</FormControl>
							<FormControl fullWidth margin="dense">
								<TextField
									type="number"
									name="price"
									label="상품가격"
									value={price}
									required
									onChange={handleChange}
									variant="outlined"
									size="small"
								/>
							</FormControl>
							<FormControl component="fieldset">
								<FormControlLabel
									control={
										<Checkbox
											type="checkbox"
											name="isStock"
											size="small"
											checked={form.isStock}
											onChange={handleChange}
										/>
									}
									label="재고사용"
								/>
							</FormControl>
						</form>
					</Grid>
				</Grid>
			</Box>
		</Container>
	);
};

export default ProductCreate;
