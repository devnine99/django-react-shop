import React, { useContext, useEffect } from 'react';
import { TaskSetStateContext } from '../../components/GlobalProvider/GlobalProvider';
import { Button } from '@material-ui/core';
import axios from 'axios';

const Test = () => {
	const setTask = useContext(TaskSetStateContext);

	const handleClick = e => {
		axios({method: 'get', url: 'http://127.0.0.1:8080/test/'})
		// 	.then(res => {
		// 		setTask(res.data.taskId);
		// 	  })
		// 	.catch(err => {
		// 		console.log(err)
		// });
	};

	return (
		<div>
			<Button onClick={handleClick}>작업 시작</Button>
		</div>
	);
};

export default Test;
