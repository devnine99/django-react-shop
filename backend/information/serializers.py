from rest_framework import serializers

from information.models import Information, Bank, SNS


class BankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bank
        fields = ['id', 'name', 'holder', 'num']


class SNSSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='get_type_display')

    class Meta:
        model = SNS
        fields = ['id', 'type', 'path']


class InformationSerializer(serializers.ModelSerializer):
    bank_set = BankSerializer(many=True)
    sns_set = SNSSerializer(many=True)

    class Meta:
        model = Information
        fields = ['site_name', 'company_name', 'owner', 'email', 'tell', 'address', 'business_license', 'mail_order_license', 'bank_set', 'sns_set']


class AboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Information
        fields = ['about_us']


class UserGuideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Information
        fields = ['user_guide']


class TermsOfServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Information
        fields = ['terms_of_service']


class PrivacyPolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = Information
        fields = ['privacy_policy']
