from rest_framework.response import Response
from rest_framework.views import APIView

from information.models import Information
from information.serializers import InformationSerializer, AboutUsSerializer, UserGuideSerializer, \
    TermsOfServiceSerializer, PrivacyPolicySerializer


class InformationView(APIView):
    def get(self, request):
        try:
            queryset = Information.objects.last()
        except Information.DoesNotExist:
            queryset = {}
        serializer = InformationSerializer(queryset)
        return Response(serializer.data)


class AboutUsView(APIView):
    def get(self, request):
        try:
            queryset = Information.objects.values('about_us').last()
        except Information.DoesNotExist:
            queryset = {}
        serializer = AboutUsSerializer(queryset)
        return Response(serializer.data)


class UserGuideView(APIView):
    def get(self, request):
        try:
            queryset = Information.objects.values('user_guide').last()
        except Information.DoesNotExist:
            queryset = {}
        serializer = UserGuideSerializer(queryset)
        return Response(serializer.data)


class TermsOfServiceView(APIView):
    def get(self, request):
        try:
            queryset = Information.objects.values('terms_of_service').last()
        except Information.DoesNotExist:
            queryset = {}
        serializer = TermsOfServiceSerializer(queryset)
        return Response(serializer.data)


class PrivacyPolicyView(APIView):
    def get(self, request):
        try:
            queryset = Information.objects.values('privacy_policy').last()
        except Information.DoesNotExist:
            queryset = {}
        serializer = PrivacyPolicySerializer(queryset)
        return Response(serializer.data)
