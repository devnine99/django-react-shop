from django.urls import path

from information.views import InformationView, AboutUsView, UserGuideView, TermsOfServiceView, PrivacyPolicyView

urlpatterns = [
    path('', InformationView.as_view()),
    path('about-us/', AboutUsView.as_view()),
    path('user-guide/', UserGuideView.as_view()),
    path('terms-of-service/', TermsOfServiceView.as_view()),
    path('privacy-policy/', PrivacyPolicyView.as_view()),
]
