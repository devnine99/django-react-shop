# Generated by Django 3.0 on 2020-05-09 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('information', '0002_auto_20200509_1739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='information',
            name='about_us',
            field=models.TextField(blank=True, null=True, verbose_name='회사소개'),
        ),
        migrations.AlterField(
            model_name='information',
            name='privacy_policy',
            field=models.TextField(blank=True, null=True, verbose_name='개인정보취급방침'),
        ),
        migrations.AlterField(
            model_name='information',
            name='terms_of_service',
            field=models.TextField(blank=True, null=True, verbose_name='서비스이용약관'),
        ),
        migrations.AlterField(
            model_name='information',
            name='user_guide',
            field=models.TextField(blank=True, null=True, verbose_name='이용안내'),
        ),
    ]
