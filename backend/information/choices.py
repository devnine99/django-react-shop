from django.db import models


class SNSTypeChoices(models.TextChoices):
    FACEBOOK = 'F', 'facebook'
    INSTAGRAM = 'I', 'instagram'
    YOUTUBE = 'Y', 'youtube'
