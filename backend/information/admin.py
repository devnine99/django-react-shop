from django.contrib import admin

from information.models import Information, Bank, SNS


class BankInline(admin.StackedInline):
    model = Bank
    extra = 0


class SNSInline(admin.StackedInline):
    model = SNS
    extra = 0


@admin.register(Information)
class InformationAdmin(admin.ModelAdmin):
    list_display = ['site_name']
    inlines = [BankInline, SNSInline]

    def has_add_permission(self, request):
        if self.model.objects.all().count() > 0:
            return False
        else:
            return True

    def has_delete_permission(self, request, obj=None):
        return False
