from django.db import models

from information.choices import SNSTypeChoices


class Information(models.Model):
    site_name = models.CharField(verbose_name='사이트명', max_length=16, null=True, blank=True)
    company_name = models.CharField(verbose_name='회사명', max_length=16, null=True, blank=True)
    owner = models.CharField(verbose_name='대표자', max_length=8, null=True, blank=True)
    email = models.EmailField(verbose_name='이메일', max_length=32, null=True, blank=True)
    tell = models.CharField(verbose_name='연락처', max_length=16, null=True, blank=True)
    address = models.CharField(verbose_name='소재지', max_length=64, null=True, blank=True)
    business_license = models.CharField(verbose_name='사업자등록번호', max_length=16, null=True, blank=True)
    mail_order_license = models.CharField(verbose_name='통신판매신고번호', max_length=16, null=True, blank=True)

    about_us = models.TextField(verbose_name='회사소개', null=True, blank=True)
    user_guide = models.TextField(verbose_name='이용안내', null=True, blank=True)
    terms_of_service = models.TextField(verbose_name='서비스이용약관', null=True, blank=True)
    privacy_policy = models.TextField(verbose_name='개인정보취급방침', null=True, blank=True)

    class Meta:
        verbose_name = '사이트 정보'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.site_name


class Bank(models.Model):
    information = models.ForeignKey('information.Information', verbose_name='사이트 정보', on_delete=models.CASCADE)

    name = models.CharField(verbose_name='계좌은행', max_length=32)
    holder = models.CharField(verbose_name='계좌주', max_length=16)
    num = models.CharField(verbose_name='계좌번호', max_length=32)

    class Meta:
        verbose_name = '무통장입금계좌'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class SNS(models.Model):
    information = models.ForeignKey('information.Information', verbose_name='사이트 정보', on_delete=models.CASCADE)

    type = models.CharField(verbose_name='타입', max_length=1, choices=SNSTypeChoices.choices)
    path = models.CharField(verbose_name='채널주소', max_length=32)

    class Meta:
        verbose_name = 'SNS'
        verbose_name_plural = verbose_name
