from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from config import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('information/', include('information.urls')),
    path('account/', include('account.urls')),
    path('celeb/', include('celeb.urls')),
    path('seller/', include('seller.urls')),
    path('post/', include('post.urls')),
    path('product/', include('product.urls')),
    path('address/', include('address.urls')),
    path('order/', include('order.urls')),
    path('', include('api.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
