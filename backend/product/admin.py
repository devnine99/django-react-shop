from django.contrib import admin

from product.models import Product, Thumbnail, Option, OptionCombination, OptionValue, Commission


class OptionValueInline(admin.StackedInline):
    model = OptionValue


@admin.register(Option)
class Option(admin.ModelAdmin):
    inlines = [OptionValueInline]


@admin.register(OptionCombination)
class OptionCombinationAdmin(admin.ModelAdmin):
    pass


class ThumbnailInline(admin.StackedInline):
    model = Thumbnail


class CommissionInline(admin.StackedInline):
    model = Commission


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    inlines = [ThumbnailInline, CommissionInline]
