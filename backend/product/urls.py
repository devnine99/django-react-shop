from django.urls import path

from product.views import ProductViewSet, ProductCartView

product_list = ProductViewSet.as_view({'get': 'list'})
product_detail = ProductViewSet.as_view({'get': 'retrieve'})

app_name = 'product'
urlpatterns = [
    path('', product_list, name='list'),
    path('<int:pk>/', product_detail, name='detail'),
    path('cart/', ProductCartView.as_view(), name='cart'),
]
