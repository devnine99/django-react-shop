from django.contrib.auth import get_user_model
from rest_framework import serializers

from celeb.models import Celeb
from product.models import Product, Thumbnail, Option, OptionValue, OptionCombination, Commission
from seller.models import Seller

User = get_user_model()


class SellerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seller
        fields = ['id', 'name']


class CelebSerializer(serializers.ModelSerializer):
    class Meta:
        model = Celeb
        fields = ['id']


class ThumbnailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thumbnail
        fields = ['id', 'thumbnail_url', 'order']


class OptionValueSerializer(serializers.ModelSerializer):
    option_name = serializers.CharField(source='option.name')

    class Meta:
        model = OptionValue
        fields = ['id', 'option_name', 'name', 'price']


class OptionSerializer(serializers.ModelSerializer):
    option_value_set = OptionValueSerializer(many=True)

    class Meta:
        model = Option
        fields = ['id', 'name', 'order', 'option_value_set']


class OptionCombinationSerializer(serializers.ModelSerializer):
    option_value_set = OptionValueSerializer(many=True)

    class Meta:
        model = OptionCombination
        fields = ['id', 'stock', 'option_value_set']


class CommissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commission
        fields = ['id', 'level', 'amount']


class ProductListSerializer(serializers.ModelSerializer):
    seller_name = serializers.CharField(source='seller.name', read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'seller_name', 'title', 'price', 'commission', 'main_thumbnail']


class ProductDetailSerializer(serializers.ModelSerializer):
    thumbnail_set = ThumbnailSerializer(many=True)
    seller = SellerSerializer()
    celeb = serializers.SerializerMethodField()
    option_set = OptionSerializer(many=True)
    option_combination_set = OptionCombinationSerializer(many=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'seller',
            'celeb',
            'main_thumbnail',
            'thumbnail_set',
            'title',
            'price',
            'is_stock',
            'option_set',
            'option_combination_set'
        ]

    def get_celeb(self, obj):
        request = self.context.get('request')
        celeb_name = request.query_params.get('celebName')
        if celeb_name:
            celeb = Celeb.objects.get(name=celeb_name)
            return celeb.id


class ProductCartSerializer(serializers.ModelSerializer):
    option_combination_set = OptionCombinationSerializer(many=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'main_thumbnail',
            'title',
            'price',
            'is_stock',
            'option_combination_set',
        ]
