from django.db import models

from celeb.choices import CelebLevelChoices


class Product(models.Model):
    seller = models.ForeignKey('seller.Seller', verbose_name='셀러', on_delete=models.CASCADE)
    celeb_set = models.ManyToManyField('celeb.Celeb', verbose_name='셀럽', related_name='product_set', through='celeb.CelebProduct')

    title = models.CharField(verbose_name='상품명', max_length=128)
    content = models.TextField(verbose_name='상품상세설명')
    price = models.PositiveSmallIntegerField(verbose_name='상품가격')

    is_stock = models.BooleanField(verbose_name='재고사용', default=False)

    created = models.DateTimeField(verbose_name='생성일', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='수정일', auto_now=True)

    class Meta:
        verbose_name = '상품'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title

    def main_thumbnail(self):
        return self.thumbnail_set.get(main=True).image.url


class Option(models.Model):
    product = models.ForeignKey('product.Product', verbose_name='상품', on_delete=models.CASCADE, related_name='option_set')

    name = models.CharField(verbose_name='옵션명', max_length=16)
    order = models.SmallIntegerField(verbose_name='순서', default=0)
    required = models.BooleanField(verbose_name='필수', default=True)

    class Meta:
        verbose_name = '옵션'
        verbose_name_plural = verbose_name
        ordering = ['order']

    def __str__(self):
        return self.name


class OptionValue(models.Model):
    option = models.ForeignKey('product.Option', verbose_name='옵션', on_delete=models.CASCADE, related_name='option_value_set')

    name = models.CharField(verbose_name='옵션값', max_length=32)
    price = models.PositiveIntegerField(verbose_name='옵션가', default=0)

    class Meta:
        verbose_name = '옵션값'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class OptionCombination(models.Model):
    product = models.ForeignKey('product.Product', verbose_name='상품', on_delete=models.CASCADE, related_name='option_combination_set')
    option_value_set = models.ManyToManyField('product.OptionValue', verbose_name='옵션값', blank=True, related_name='option_combination_set')

    stock = models.PositiveIntegerField(verbose_name='재고', default=0)
    is_active = models.BooleanField(verbose_name='노출', default=True)

    class Meta:
        verbose_name = '옵션조합'
        verbose_name_plural = verbose_name

    def get_price(self):
        return self.product.price + sum(option_value.price for option_value in self.option_value_set.all())

    def get_commission(self, celeb_level):
        return self.product.price * self.product.commission_set.get(level=celeb_level) / 100


class Thumbnail(models.Model):
    product = models.ForeignKey('product.Product', verbose_name='썸네일', on_delete=models.CASCADE)

    image = models.ImageField(verbose_name='이미지', upload_to='product/%Y/%m/%d')
    main = models.BooleanField(verbose_name='메인', default=False)
    order = models.PositiveSmallIntegerField(verbose_name='순서', default=0)

    class Meta:
        verbose_name = '썸네일'
        verbose_name_plural = verbose_name

    def thumbnail_url(self):
        return self.image.url


class Commission(models.Model):
    product = models.ForeignKey('product.Product', verbose_name='상품', on_delete=models.CASCADE)
    level = models.CharField(verbose_name='레벨', max_length=2, choices=CelebLevelChoices.choices)
    amount = models.DecimalField(verbose_name='양', max_digits=2, decimal_places=0)

    class Meta:
        verbose_name = '커미션'
        verbose_name_plural = verbose_name
