from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from celeb.permissions import IsCelebOnly
from product.models import Product
from product.serializers import ProductListSerializer, ProductDetailSerializer, ProductCartSerializer
from seller.permissions import IsSellerOnly


class ProductViewSet(ModelViewSet):
    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            return [IsCelebOnly()]
        elif self.action in ['create', 'update']:
            return [IsSellerOnly()]

    def get_queryset(self):
        if self.action in ['list']:
            return Product.objects.all()
        elif self.action in ['retrieve']:
            return Product.objects.prefetch_related('option_set', 'option_combination_set').all()

    def get_serializer_class(self):
        if self.action in ['list']:
            return ProductListSerializer
        elif self.action in ['retrieve']:
            return ProductDetailSerializer


class ProductCartView(GenericAPIView):
    serializer_class = ProductCartSerializer

    def get_queryset(self):
        ids = self.request.query_params.get('ids').split(',')
        return Product.objects.prefetch_related('option_combination_set').filter(id__in=ids)

    def get(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
