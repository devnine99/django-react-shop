## Account API
### Login API
##### ENDPOINT
~~~
POST
/account/login
~~~

##### DATA
|FIELD|TYPE|REQUIRED|
|:---:|:---:|:---:|
|username|str|true|
|password|str|true|

##### RESPONSE
~~~
{
    "key": "KEY VALUE"
}
~~~



### Logout API
##### ENDPOINT
~~~
POST
/account/logout
~~~


### Register API
##### ENDPOINT
~~~
POST
/account/register
~~~

##### DATA
|FIELD|TYPE|REQUIRED|
|:---:|:---:|:---:|
|username|str|true|
|email|str|true|
|password1|str|true|
|password2|str|true|
|is_privacy_policy|bool|true|
|is_terms_of_service|bool|true|

##### RESPONSE
~~~
{
    "key": "KEY VALUE"
}
~~~



### Profile API
##### ENDPOINT
~~~
GET, PATCH
/account/profile
~~~

##### DATA
|FIELD|TYPE|REQUIRED|
|:---:|:---:|:---:|
|email|str|true|



## Celeb API

## Seller API

## Product API

## Order API
