from django.urls import path

from order.views import OrderCreateView, OrderCompleteView

app_name = 'order'
urlpatterns = [
    path('create/', OrderCreateView.as_view()),
    path('complete/', OrderCompleteView.as_view()),
    # path('hook/', OrderHookView.as_view()),
    # path('<merchant_uid>/', OrderDetailView.as_view()),
]
