from iamport import Iamport
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from order.serializers import OrderCreateSerializer, OrderCompleteSerializer, OrderDetailSerializer


# class OrderDetailView(GenericAPIView):
#     serializer_class = OrderDetailSerializer
#
#     def get(self, request):
#         serializer = self.get_serializer()
#         return Response(serializer.data)


class OrderCreateView(GenericAPIView):
    serializer_class = OrderCreateSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = serializer.save()

        return Response({'merchant_uid': order.merchant_uid})


class OrderCompleteView(GenericAPIView):
    serializer_class = OrderCompleteSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = serializer.save()
        serializer = OrderDetailSerializer(order)

        return Response(serializer.data)


# class OrderHookView(GenericAPIView):
#     serializer_class = OrderHookSerializer
#
#     def post(self, request):
#         serializer = self.get_serializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#
#         return Response({'message': '정상 처리 되었습니다.'})
