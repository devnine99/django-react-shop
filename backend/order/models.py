from django.db import models

from order.choices import OrderStatusChoices


class Order(models.Model):
    merchant_uid = models.CharField(verbose_name='주문ID', max_length=32, unique=True)
    # imp_uid = models.CharField(verbose_name='imp ID', max_length=32, unique=True)
    order_name = models.CharField(verbose_name='주문자 성함', max_length=8)
    order_phone = models.CharField(verbose_name='주문자 연락처', max_length=16)
    order_email = models.EmailField(verbose_name='주문자 이메일')
    order_postcode = models.PositiveIntegerField(verbose_name='주문자 우편번호')
    order_address1 = models.CharField(verbose_name='주문자 기본주소', max_length=128)
    order_address2 = models.CharField(verbose_name='주문자 상세주소', max_length=128)

    ship_name = models.CharField(verbose_name='배송지 성함', max_length=8)
    ship_phone1 = models.CharField(verbose_name='배송지 연락처1', max_length=16)
    ship_phone2 = models.CharField(verbose_name='배송지 연락처2', max_length=16, null=True, blank=True)
    ship_postcode = models.PositiveIntegerField(verbose_name='배송지 우편번호')
    ship_address1 = models.CharField(verbose_name='배송지 기본주소', max_length=128)
    ship_address2 = models.CharField(verbose_name='배송지 상세주소', max_length=128)
    ship_memo = models.CharField(verbose_name='배송지 메모', max_length=256, null=True, blank=True)

    pay_method = models.CharField(verbose_name='결제방법', max_length=16)
    amount = models.IntegerField(verbose_name='결제금액', default=0)
    status = models.CharField(verbose_name='결제상태', max_length=16, choices=OrderStatusChoices.choices, default=OrderStatusChoices.READY)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = '주문'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.merchant_uid

    def get_total_price(self):
        return sum(item.get_item_price() for item in self.order_item_set.all())


class OrderItem(models.Model):
    order = models.ForeignKey('order.Order', verbose_name='주문', on_delete=models.CASCADE, related_name='order_item_set')
    product = models.ForeignKey('product.Product', verbose_name='상품', on_delete=models.PROTECT, related_name='order_item_set')
    celeb = models.ForeignKey('celeb.Celeb', verbose_name='셀럽', on_delete=models.PROTECT, related_name='order_item_set')
    option_combination = models.ForeignKey('product.OptionCombination', verbose_name='콤비네이션', on_delete=models.PROTECT)
    title = models.CharField(verbose_name='상품명', max_length=128)
    option = models.CharField(verbose_name='상품옵션', max_length=128, null=True, blank=True)
    price = models.IntegerField(verbose_name='가격')
    quantity = models.PositiveIntegerField(verbose_name='수량')

    class Meta:
        verbose_name = '주문상품'
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.id)

    def get_item_price(self):
        return self.price * self.quantity


class VBank(models.Model):
    order = models.OneToOneField('order.Order', verbose_name='주문', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='가상계좌은행', max_length=32)
    holder = models.CharField(verbose_name='가상계좌주', max_length=16)
    num = models.CharField(verbose_name='가상계좌번호', max_length=32)
    date = models.DateTimeField(verbose_name='가상계좌기한')

    class Meta:
        verbose_name = '가상계좌'
        verbose_name_plural = verbose_name
