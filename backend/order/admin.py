from django.contrib import admin

from order.models import Order, OrderItem, VBank


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    extra = 0


class VBankInline(admin.StackedInline):
    model = VBank


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'merchant_uid', 'amount']
    inlines = [OrderItemInline, VBankInline]
