from django.db import models


class OrderStatusChoices(models.TextChoices):
    READY = 'ready', '결제대기'
    PAID = 'paid', '결제완료'
    CANCELLED = 'cancelled', '결제취소'
    FAILED = 'failed', '결제실패'
