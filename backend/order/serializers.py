from datetime import date, datetime

from django.db import transaction
from iamport import Iamport
from rest_framework import serializers

from order.models import Order, OrderItem, VBank
from product.models import OptionCombination
from settings import IMP_KEY, IMP_SECRET


class OrderItemListSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ['product', 'celeb', 'title', 'option', 'price', 'option_combination', 'quantity']


class VBankDetailSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format='%Y년 %m월 %d일')

    class Meta:
        model = VBank
        fields = ['name', 'holder', 'num', 'date']


class OrderDetailSerializer(serializers.ModelSerializer):
    order_item_set = OrderItemListSerializer(many=True)
    vbank = VBankDetailSerializer()

    class Meta:
        model = Order
        fields = [
            'merchant_uid',
            'order_name',
            'order_phone',
            'order_email',
            'order_postcode',
            'order_address1',
            'order_address2',
            'ship_name',
            'ship_phone1',
            'ship_phone2',
            'ship_postcode',
            'ship_address1',
            'ship_address2',
            'ship_memo',
            'amount',
            'pay_method',
            'status',
            'order_item_set',
            'vbank',
        ]


class CombinationSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionCombination
        fields = ['id']


class OrderItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ['product', 'celeb', 'title', 'option', 'option_combination', 'quantity']


class OrderCreateSerializer(serializers.ModelSerializer):
    items = OrderItemCreateSerializer(many=True, write_only=True)

    class Meta:
        model = Order
        fields = [
            'order_name',
            'order_phone',
            'order_email',
            'order_postcode',
            'order_address1',
            'order_address2',
            'ship_name',
            'ship_phone1',
            'ship_phone2',
            'ship_postcode',
            'ship_address1',
            'ship_address2',
            'ship_memo',
            'pay_method',
            'items',
        ]
        read_only_fields = ['merchant_uid']

    @transaction.atomic
    def create(self, validated_data):
        today = date.today().strftime('%Y%m%d')
        last_order = Order.objects.filter(merchant_uid__startswith=today).last()
        if last_order:
            last_number = int(last_order.merchant_uid[-8:])
        else:
            last_number = 0

        new_merchant_uid = f'{today}-{"%08d" % (last_number + 1)}'

        items = validated_data.pop('items')
        new_order = Order.objects.create(merchant_uid=new_merchant_uid, **validated_data)
        for item in items:
            product = item['product']
            option_combination = item['option_combination']
            quantity = item['quantity']
            OrderItem.objects.create(
                order=new_order,
                price=option_combination.get_price(),
                **item,
            )
            # # 재고 사용 시 => 주문수량만큼 재고 빼기
            if product.is_stock:
                option_combination.stock -= quantity
                option_combination.save()
        new_order.amount = new_order.get_total_price()
        new_order.save()
        return new_order


class OrderCompleteSerializer(serializers.Serializer):
    merchant_uid = serializers.CharField()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.iamport = Iamport(imp_key=IMP_KEY, imp_secret=IMP_SECRET)
        self.order = None
        self.payment = None

    def validate(self, attrs):
        merchant_uid = attrs.get('merchant_uid')

        try:
            self.order = Order.objects.prefetch_related('order_item_set').get(merchant_uid=merchant_uid)
        except Order.DoesNotExist:
            raise ValueError({'error': '존재하지 않는 주문입니다.'})

        if not self.order.pay_method == 'cash':
            self.payment = self.iamport.find(merchant_uid=merchant_uid)
            if not self.payment['amount'] == self.order.amount:
                self.order.status = 'cancelled'
                self.iamport.cancel('결제금액과 주문금액이 일치하지 않습니다.', merchant_uid=merchant_uid)
                raise ValueError({'error': '결제금액과 주문금액이 일치하지 않아 결제가 취소 되었습니다.'})

        return attrs

    @transaction.atomic
    def create(self, validated_data):
        if not self.order.pay_method == 'cash':
            self.order.status = self.payment['status']
            if self.order.pay_method == 'vbank':
                VBank.objects.update_or_create(
                    order=self.order,
                    defaults={
                        'name': self.payment['vbank_name'],
                        'holder': self.payment['vbank_holder'],
                        'num': self.payment['vbank_num'],
                        'date': datetime.fromtimestamp(self.payment['vbank_date']),
                    },
                )
        self.order.save()

        return self.order
