from django.urls import path

from post.views import PostViewSet, PostCommentViewSet, PostLikeAPIView

post_list = PostViewSet.as_view({'get': 'list'})
post_detail = PostViewSet.as_view({'get': 'retrieve'})

post_comment_list = PostCommentViewSet.as_view({'get': 'list', 'post': 'create'})

post_like_detail = PostLikeAPIView.as_view()


urlpatterns = [
    path('', post_list),
    path('<int:pk>/', post_detail),
    path('<int:post_pk>/comment/', post_comment_list),
    path('<int:post_pk>/like/', post_like_detail),
]
