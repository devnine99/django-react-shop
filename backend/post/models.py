from django.db import models


class Post(models.Model):
    user = models.ForeignKey('account.User', verbose_name='유저', on_delete=models.CASCADE)
    like_user = models.ManyToManyField('account.User', related_name='like_post', blank=True)
    content = models.TextField(verbose_name='내용')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = '포스트'
        verbose_name_plural = verbose_name
        ordering = ['-created']

    def __str__(self):
        return str(self.pk)

    def like_count(self):
        return self.like_user.count()

    def comment_count(self):
        return self.comment_set.count()


class Media(models.Model):
    post = models.ForeignKey('post.Post', verbose_name='포스트', on_delete=models.CASCADE)

    file = models.FileField(verbose_name='파일', upload_to='post/%Y')
    order = models.PositiveSmallIntegerField(verbose_name='순서')


class Comment(models.Model):
    user = models.ForeignKey('account.User', verbose_name='유저', on_delete=models.CASCADE)
    post = models.ForeignKey('post.Post', verbose_name='포스트', on_delete=models.CASCADE)

    content = models.CharField(verbose_name='내용', max_length=256)
