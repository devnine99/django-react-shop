from django.contrib.auth import get_user_model
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from post.models import Post, Comment
from post.serializers import PostDetailSerializer, CommentListSerializer, LikeSerializer

User = get_user_model()


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_serializer_class(self):
        if self.action in ['list']:
            return PostDetailSerializer
        elif self.action in ['retrieve']:
            return PostDetailSerializer


class PostCommentViewSet(ModelViewSet):
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        return Comment.objects.filter(post=self.kwargs.get('post_pk'))

    def get_serializer_class(self):
        if self.action in ['list', 'create']:
            return CommentListSerializer

    def create(self, request, *args, **kwargs):
        request.data['user'] = request.user.id
        return super().create(request, *args, **kwargs)


class PostLikeAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def patch(self, request, post_pk):
        post = Post.objects.get(pk=post_pk)

        if post in request.user.like_post.all():
            request.user.like_post.remove(post)
        else:
            request.user.like_post.add(post)

        serializer = LikeSerializer(request.user)
        return Response(serializer.data)
