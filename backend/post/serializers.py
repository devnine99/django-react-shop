from django.contrib.auth import get_user_model
from rest_framework.fields import SerializerMethodField, CharField
from rest_framework.serializers import ModelSerializer

from post.models import Post, Media, Comment

User = get_user_model()


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']


class LikeSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['like_post']


class MediaSerializer(ModelSerializer):
    class Meta:
        model = Media
        fields = ['id', 'file', 'order']


class CommentListSerializer(ModelSerializer):
    username = CharField(source='user.username', read_only=True)

    class Meta:
        model = Comment
        fields = ['id', 'user', 'username', 'post', 'content']


class PostListSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'media_set']


class PostDetailSerializer(ModelSerializer):
    username = CharField(source='user.username', read_only=True)
    media_set = MediaSerializer(many=True)
    comment_set = CommentListSerializer(many=True)
    liked = SerializerMethodField(default=False)

    class Meta:
        model = Post
        fields = [
            'id',
            'username',
            'content',
            'created',
            'media_set',
            'liked',
            'like_count',
            'comment_set',
            'comment_count'
        ]

    def get_liked(self, obj):
        user_id = self.context.get('request')
        if user_id:
            if user_id.user.id in obj.like_user.values_list('id', flat=True):
                return True
            else:
                return False
        else:
            return False
