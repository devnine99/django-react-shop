from django.contrib import admin

from post.models import Post, Comment, Media


class MediaInline(admin.StackedInline):
    model = Media


class CommentInline(admin.StackedInline):
    model = Comment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    inlines = [MediaInline, CommentInline]
