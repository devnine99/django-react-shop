import requests
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class AddressView(APIView):
    def get(self, request):
        """U01TX0FVVEgyMDIwMDIyMDAwNTU0MDEwOTQ4MTQ="""
        page = request.GET.get('page')
        keyword = request.GET.get('keyword')

        url = 'http://www.juso.go.kr/addrlink/addrLinkApi.do'
        params = {
            'confmKey': 'devU01TX0FVVEgyMDIwMDIyODAwMDIxNzEwOTUwMjY=',
            'resultType': 'json',
            'countPerPage': '10',
            'currentPage': page,
            'keyword': keyword
        }

        response = requests.get(url, params)
        if not response.ok:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        results = response.json().get('results')
        total = int(results['common']['totalCount'])
        if total == 0:
            return Response({'keyword': ['검색 결과가 없습니다.']}, status=status.HTTP_400_BAD_REQUEST)

        juso = results['juso']
        address = [{'id': r['bdMgtSn'], 'postcode': r['zipNo'], 'roadAddress': r['roadAddr'], 'jibunAddress': r['jibunAddr']} for r in juso]
        data = {
            'total': total,
            'address': address
        }
        return Response(data)
