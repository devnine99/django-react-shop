from django.urls import path

from address.views import AddressView

app_name = 'address'
urlpatterns = [
    path('', AddressView.as_view(), name='address'),
]
