from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from account.validators import validate_password, validate_username
from product.models import Product, Commission, Thumbnail, OptionValue, Option, OptionCombination
from seller.models import Seller

User = get_user_model()


class SellerListSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')

    class Meta:
        model = Seller
        fields = [
            'id',
            'name',
            'username',
            'cover_image',
            'profile_image',
        ]


class SellerRegisterSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    email = serializers.EmailField(write_only=True)
    phone = serializers.CharField(write_only=True)
    name = serializers.CharField(write_only=True)
    is_privacy_policy = serializers.BooleanField(write_only=True)
    is_terms_of_service = serializers.BooleanField(write_only=True)

    def validate(self, attrs):
        username = attrs['username']
        email = attrs['email']
        password1 = attrs['password1']
        password2 = attrs['password2']
        is_privacy_policy = attrs['is_privacy_policy']
        is_terms_of_service = attrs['is_terms_of_service']

        errors = dict()
        if User.objects.filter(username=username).exists():
            errors['username'] = ['이미 존재하는 유저입니다.']
        else:
            username_error = validate_username(username)
            errors.update(username_error)
        if password1 != password2:
            errors['password1'] = ['비밀번호가 일치하지 않습니다.']
            errors['password2'] = ['비밀번호가 일치하지 않습니다.']
        else:
            password_error = validate_password(password1, password2)
            errors.update(password_error)
        if not is_privacy_policy:
            errors['is_privacy_policy'] = ['개인정보취급방침에 동의하셔야 합니다.']
        if not is_terms_of_service:
            errors['is_terms_of_service'] = ['서비스이용약관에 동의하셔야 합니다.']
        if errors:
            raise ValidationError(errors)

        return attrs

    @transaction.atomic
    def create(self, validated_data):
        validated_data.pop('password2')
        validated_data['password'] = validated_data.pop('password1')
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
            email=validated_data['email'],
            is_privacy_policy=validated_data['is_privacy_policy'],
            is_terms_of_service=validated_data['is_terms_of_service'],
            is_active=False,
        )
        seller = Seller.objects.create(
            user=user,
            name=validated_data['name'],
            phone=validated_data['phone'],
        )
        return seller


class SellerDetailSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')

    class Meta:
        model = Seller
        fields = [
            'id',
            'name',
            'username',
            'cover_image',
            'profile_image',
        ]


class CommissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commission
        fields = ['id', 'level', 'amount']


class SellerProductListSerializer(serializers.ModelSerializer):
    seller_name = serializers.CharField(source='seller.name')
    commission_set = CommissionSerializer(many=True)

    class Meta:
        model = Product
        fields = ['id', 'seller_name', 'title', 'price', 'commission_set', 'main_thumbnail']


class ThumbnailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thumbnail
        fields = ['id', 'thumbnail_url', 'order']


class OptionValueSerializer(serializers.ModelSerializer):
    option_name = serializers.CharField(source='option.name')

    class Meta:
        model = OptionValue
        fields = ['id', 'option_name', 'name', 'price']


class OptionSerializer(serializers.ModelSerializer):
    option_value_set = OptionValueSerializer(many=True)

    class Meta:
        model = Option
        fields = ['id', 'name', 'order', 'option_value_set']


class OptionCombinationSerializer(serializers.ModelSerializer):
    option_value_set = OptionValueSerializer(many=True)

    class Meta:
        model = OptionCombination
        fields = ['id', 'stock', 'option_value_set']


class SellerProductDetailSerializer(serializers.ModelSerializer):
    thumbnail_set = ThumbnailSerializer(many=True)
    seller_name = serializers.CharField(source='seller.name')
    option_set = OptionSerializer(many=True)
    option_combination_set = OptionCombinationSerializer(many=True)
    commission_set = CommissionSerializer(many=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'seller_name',
            'main_thumbnail',
            'thumbnail_set',
            'title',
            'price',
            'is_stock',
            'option_set',
            'option_combination_set',
            'commission_set',
        ]
