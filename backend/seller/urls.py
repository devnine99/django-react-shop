from django.urls import path

from seller.views import SellerViewSet, SellerProductViewSet


app_name = 'seller'
urlpatterns = [
    path('', SellerViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    path('<seller_name>/', SellerViewSet.as_view({'get': 'retrieve'}), name='detail'),
    path('<seller_name>/product/', SellerProductViewSet.as_view({'get': 'list'})),
    path('<seller_name>/product/<product_id>/', SellerProductViewSet.as_view({'get': 'retrieve'})),
]
