from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver


class Seller(models.Model):
    user = models.OneToOneField('account.User', verbose_name='유저', on_delete=models.CASCADE)

    cover_image = models.ImageField(verbose_name='커버이미지', upload_to='seller/cover', blank=True)
    profile_image = models.ImageField(verbose_name='프로필이미지', upload_to='seller/profile', blank=True)
    name = models.CharField(verbose_name='셀러명', max_length=16)
    phone = models.CharField(verbose_name='연락처', max_length=16)

    class Meta:
        verbose_name = '셀러'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    @transaction.atomic
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class Deposit(models.Model):
    seller = models.OneToOneField('seller.Seller', verbose_name='셀러', on_delete=models.CASCADE, related_name='deposit')

    pending = models.PositiveIntegerField(verbose_name='보류', default=0)
    approved = models.PositiveIntegerField(verbose_name='승인', default=0)

    class Meta:
        verbose_name = '예치금'
        verbose_name_plural = verbose_name


@receiver(post_save, sender=Seller)
def create_deposit(sender, instance, created, *args, **kwargs):
    if created:
        Deposit.objects.create(seller=instance)
