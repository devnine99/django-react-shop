from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ModelViewSet

from celeb.permissions import IsCelebOnly, IsCelebOrOwnerOnly
from seller.models import Seller
from seller.serializers import SellerListSerializer, SellerDetailSerializer, SellerProductListSerializer, \
    SellerProductDetailSerializer, SellerRegisterSerializer


class SellerViewSet(ModelViewSet):
    queryset = Seller.objects.all()
    lookup_field = 'name'
    lookup_url_kwarg = 'seller_name'
    pagination_class = PageNumberPagination

    def get_permissions(self):
        if self.action in ['list']:
            return [IsCelebOnly()]
        elif self.action in ['create']:
            return [AllowAny()]
        elif self.action in ['retrieve']:
            return [IsCelebOrOwnerOnly()]

    def get_serializer_class(self):
        if self.action in ['list']:
            return SellerListSerializer
        elif self.action in ['create']:
            return SellerRegisterSerializer
        elif self.action in ['retrieve']:
            return SellerDetailSerializer


class SellerProductViewSet(ModelViewSet):
    permission_classes = [IsCelebOrOwnerOnly]
    lookup_url_kwarg = 'product_id'

    def get_queryset(self):
        seller_name = self.kwargs['seller_name']
        seller = Seller.objects.get(name=seller_name)
        return seller.product_set.all()

    def get_serializer_class(self):
        if self.action in ['list']:
            return SellerProductListSerializer
        elif self.action in ['retrieve']:
            return SellerProductDetailSerializer
