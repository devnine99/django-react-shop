from django.contrib import admin

from seller.models import Seller, Deposit


class DepositInline(admin.StackedInline):
    model = Deposit


@admin.register(Seller)
class SellerAdmin(admin.ModelAdmin):
    inlines = [DepositInline]
