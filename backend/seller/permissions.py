from rest_framework.permissions import BasePermission


class IsSellerOnly(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.seller:
            return True
        return False
