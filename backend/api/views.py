from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from celeb.models import Celeb
from product.models import Product
from seller.models import Seller


class APIRootView(APIView):
    def get(self, request):
        product = Product.objects.first()
        celeb = Celeb.objects.first()
        seller = Seller.objects.first()

        data = {
            'account': {
                'login': reverse('account:login', request=request),
                'logout': reverse('account:logout', request=request),
                'register': reverse('account:register', request=request),
                'profile': reverse('account:profile', request=request),
                'password_reset': reverse('account:password_reset', request=request),
                'password_reset_confirm': reverse('account:password_reset_confirm', request=request),
            },
            'product': {
                'list': reverse('product:list', request=request),
                'detail': reverse('product:detail', args=[product.id], request=request),
                'cart': reverse('product:cart', request=request),
            },
            'celeb': {
                'list': reverse('celeb:list', request=request),
                'detail': reverse('celeb:detail', args=[celeb.user.username], request=request),
            },
            'seller': {
                'list': reverse('seller:list', request=request),
                'detail': reverse('seller:detail', args=[seller.user.username], request=request),
            },
        }
        return Response(data)
