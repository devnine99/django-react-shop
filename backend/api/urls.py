from django.urls import path

from api.views import APIRootView

urlpatterns = [
    path('', APIRootView.as_view()),
]
