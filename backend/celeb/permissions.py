from rest_framework.permissions import BasePermission

from celeb.models import Celeb


class IsCelebOnly(BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_anonymous and Celeb.objects.filter(user=request.user).exists():
            return True
        return False


class IsCelebOrOwnerOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_anonymous and Celeb.objects.filter(user=request.user).exists():
            return True
        elif obj.user == request.user:
            return True
        return False


class IsCelebOwnerOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        if not request.user.is_anonymous and obj.celeb.user == request.user:
            return True
        return False