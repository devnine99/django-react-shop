from django.apps import AppConfig


class CelebConfig(AppConfig):
    name = 'celeb'
