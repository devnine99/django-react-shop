from django.contrib import admin

from celeb.models import Celeb, SNS, Deposit


class SNSInline(admin.StackedInline):
    model = SNS
    extra = 0


class DepositInline(admin.StackedInline):
    model = Deposit


@admin.register(Celeb)
class CelebAdmin(admin.ModelAdmin):
    inlines = [SNSInline, DepositInline]
