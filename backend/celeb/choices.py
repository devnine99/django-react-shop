from django.db import models


class CelebLevelChoices(models.TextChoices):
    SS = 'SS'
    S = 'S'
    A = 'A'
    B = 'B'


class SNSTypeChoices(models.TextChoices):
    FACEBOOK = 'F', 'facebook'
    INSTAGRAM = 'I', 'instagram'
    YOUTUBE = 'Y', 'youtube'
