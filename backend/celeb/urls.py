from django.urls import path

from celeb.views import CelebViewSet, CelebProductViewSet, CelebCelebProductViewSet, CelebProfileView

app_name = 'celeb'
urlpatterns = [
    path('', CelebViewSet.as_view({'get': 'list', 'post': 'create'}), name='list'),
    path('celeb_product/', CelebCelebProductViewSet.as_view({'post': 'create'})),
    path('celeb_product/<product_id>/', CelebCelebProductViewSet.as_view({'delete': 'destroy'})),
    path('profile/', CelebProfileView.as_view()),

    # path가 celeb_name으로 들어가는 것을 주의하기 위해 아래 배치!
    path('<celeb_name>/', CelebViewSet.as_view({'get': 'retrieve'}), name='detail'),
    path('<celeb_name>/product/', CelebProductViewSet.as_view({'get': 'list'})),
    path('<celeb_name>/product/<product_id>/', CelebProductViewSet.as_view({'get': 'retrieve'})),
]
