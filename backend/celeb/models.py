from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver

from celeb.choices import SNSTypeChoices, CelebLevelChoices


class Celeb(models.Model):
    user = models.OneToOneField('account.User', verbose_name='유저', on_delete=models.CASCADE)
    level = models.CharField(verbose_name='등급', max_length=2, choices=CelebLevelChoices.choices, default=CelebLevelChoices.B)
    follower = models.ManyToManyField('account.User', verbose_name='팔로워', blank=True, related_name='following')

    name = models.CharField(verbose_name='셀럽명', max_length=8)
    phone = models.CharField(verbose_name='연락처', max_length=16)

    cover_image = models.ImageField(verbose_name='커버이미지', upload_to='seller/cover', blank=True)
    profile_image = models.ImageField(verbose_name='프로필이미지', upload_to='seller/profile', blank=True)

    class Meta:
        verbose_name = '셀럽'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    @transaction.atomic
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def get_product_set(self):
        return self.product_set.order_by('celebproduct')


class SNS(models.Model):
    celeb = models.ForeignKey('celeb.Celeb', verbose_name='셀럽', on_delete=models.CASCADE)

    type = models.CharField(verbose_name='타입', max_length=1, choices=SNSTypeChoices.choices)
    path = models.CharField(verbose_name='채널주소', max_length=32)

    class Meta:
        verbose_name = 'SNS'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '{}: {}'.format(self.type, self.path)


class Deposit(models.Model):
    celeb = models.OneToOneField('celeb.Celeb', verbose_name='셀럽', on_delete=models.CASCADE)

    pending = models.PositiveIntegerField(verbose_name='보류', default=0)
    approved = models.PositiveIntegerField(verbose_name='승인', default=0)

    class Meta:
        verbose_name = '예치금'
        verbose_name_plural = verbose_name


class CelebProduct(models.Model):
    celeb = models.ForeignKey('celeb.Celeb', verbose_name='셀럽', on_delete=models.CASCADE)
    product = models.ForeignKey('product.Product', verbose_name='상품', on_delete=models.CASCADE)
    ordering = models.PositiveSmallIntegerField(verbose_name='순서')

    class Meta:
        db_table = 'celeb_celeb_product'
        ordering = ['ordering']


@receiver(post_save, sender=Celeb)
def create_deposit(sender, instance, created, *args, **kwargs):
    if created:
        Deposit.objects.create(celeb=instance)
