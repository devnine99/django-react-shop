from django.db.models import Q
from rest_framework.generics import UpdateAPIView, GenericAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from celeb.models import Celeb, CelebProduct
from celeb.permissions import IsCelebOnly, IsCelebOwnerOnly
from celeb.serializers import CelebListSerializer, CelebDetailSerializer, CelebProductListSerializer, \
    CelebProductDetailSerializer, CelebProductOrderingSerializer, CelebRegisterSerializer, CelebCelebProductSerializer, \
    CelebProfileSerializer
from product.models import Product


class CelebViewSet(ModelViewSet):
    queryset = Celeb.objects.all()
    lookup_field = 'name'
    lookup_url_kwarg = 'celeb_name'
    pagination_class = PageNumberPagination

    def get_queryset(self):
        keyword = self.request.query_params.get('keyword')
        if keyword:
            return Celeb.objects.filter(Q(name__contains=keyword)|Q(user__username__contains=keyword))
        else:
            return Celeb.objects.all()

    def get_serializer_class(self):
        if self.action in ['list']:
            return CelebListSerializer
        elif self.action in ['create']:
            return CelebRegisterSerializer
        elif self.action in ['retrieve']:
            return CelebDetailSerializer


class CelebProductViewSet(ModelViewSet):
    lookup_url_kwarg = 'product_id'

    def get_queryset(self):
        celeb_name = self.kwargs['celeb_name']
        celeb = Celeb.objects.get(name=celeb_name)
        return celeb.get_product_set()

    def get_permissions(self):
        if self.action in ['create', 'destroy']:
            return [IsCelebOnly()]

    def get_serializer_class(self):
        if self.action in ['list']:
            return CelebProductListSerializer
        elif self.action in ['retrieve']:
            return CelebProductDetailSerializer


class CelebCelebProductViewSet(ModelViewSet):
    lookup_field = 'product'
    lookup_url_kwarg = 'product_id'
    permission_classes = [IsCelebOwnerOnly]
    serializer_class = CelebCelebProductSerializer

    def get_queryset(self):
        return CelebProduct.objects.filter(celeb=self.request.user.celeb)


# class CelebProductOrderingView(GenericAPIView):
#     queryset = CelebProduct.objects.all()
#     serializer_class = CelebProductOrderingSerializer
#     permission_classes = [IsCelebOnly]
#
#     def post(self, request):
#         serializer = self.get_serializer(request.user.celeb, data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#
#         return Response(serializer.data)


class CelebProfileView(RetrieveAPIView):
    serializer_class = CelebProfileSerializer
    permission_classes = [IsCelebOwnerOnly]

    def get_object(self):
        celeb = self.request.user.celeb
        return celeb
