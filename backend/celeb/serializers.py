from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField

from account.validators import validate_username, validate_password
from celeb.models import Celeb, SNS, CelebProduct, Deposit
from product.models import Product, OptionValue, Option, OptionCombination, Thumbnail

User = get_user_model()


class SNSListSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='get_type_display')

    class Meta:
        model = SNS
        fields = ['id', 'type', 'path']


class SNSCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = SNS
        fields = ['type', 'path']


class DepositSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField()

    class Meta:
        model = Deposit
        fields = ['pending', 'approved', 'total']

    def get_total(self, obj):
        return obj.pending + obj.approved


class CelebListSerializer(serializers.ModelSerializer):
    sns_set = SNSListSerializer(many=True)
    username = CharField(source='user.username')
    product_count = serializers.SerializerMethodField()
    post_count = serializers.SerializerMethodField()
    follower_count = serializers.SerializerMethodField()

    class Meta:
        model = Celeb
        fields = [
            'id',
            'sns_set',
            'name',
            'username',
            'cover_image',
            'profile_image',
            'product_count',
            'post_count',
            'follower_count',
        ]

    def get_product_count(self, obj):
        if hasattr(obj, 'product_set'):
            return obj.product_set.count()
        else:
            return 0

    def get_post_count(self, obj):
        if hasattr(obj, 'post_set'):
            return obj.post_set.count()
        else:
            return 0

    def get_follower_count(self, obj):
        return obj.follower.count()


class CelebDetailSerializer(serializers.ModelSerializer):
    sns_set = SNSListSerializer(many=True)
    username = CharField(source='user.username')
    product_count = serializers.SerializerMethodField()
    post_count = serializers.SerializerMethodField()
    follower_count = serializers.SerializerMethodField()
    followed = serializers.SerializerMethodField()

    class Meta:
        model = Celeb
        fields = [
            'id',
            'sns_set',
            'name',
            'username',
            'cover_image',
            'profile_image',
            'product_count',
            'post_count',
            'follower_count',
            'followed',
        ]

    def get_product_count(self, obj):
        if hasattr(obj, 'product_set'):
            return obj.product_set.count()
        else:
            return 0

    def get_post_count(self, obj):
        if hasattr(obj, 'post_set'):
            return obj.post_set.count()
        else:
            return 0

    def get_follower_count(self, obj):
        return obj.follower.count()

    def get_followed(self, obj):
        user = self.context['request'].user
        if user in obj.follower.all():
            return True
        else:
            return False


class CelebProfileSerializer(serializers.ModelSerializer):
    sns_set = SNSListSerializer(many=True)
    username = CharField(source='user.username')
    product_count = serializers.SerializerMethodField()
    post_count = serializers.SerializerMethodField()
    follower_count = serializers.SerializerMethodField()
    deposit = DepositSerializer()

    class Meta:
        model = Celeb
        fields = [
            'id',
            'sns_set',
            'name',
            'username',
            'cover_image',
            'profile_image',
            'product_count',
            'post_count',
            'follower_count',
            'deposit',
        ]

    def get_product_count(self, obj):
        if hasattr(obj, 'product_set'):
            return obj.product_set.count()
        else:
            return 0

    def get_post_count(self, obj):
        if hasattr(obj, 'post_set'):
            return obj.post_set.count()
        else:
            return 0

    def get_follower_count(self, obj):
        return obj.follower.count()


class CelebRegisterSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    email = serializers.EmailField(write_only=True)
    phone = serializers.CharField(write_only=True)
    name = serializers.CharField(write_only=True)
    sns_set = SNSCreateSerializer(many=True, write_only=True)
    is_privacy_policy = serializers.BooleanField(write_only=True)
    is_terms_of_service = serializers.BooleanField(write_only=True)

    def validate(self, attrs):
        username = attrs['username']
        email = attrs['email']
        password1 = attrs['password1']
        password2 = attrs['password2']
        is_privacy_policy = attrs['is_privacy_policy']
        is_terms_of_service = attrs['is_terms_of_service']

        errors = dict()
        if User.objects.filter(username=username).exists():
            errors['username'] = ['이미 존재하는 유저입니다.']
        else:
            username_error = validate_username(username)
            errors.update(username_error)
        if password1 != password2:
            errors['password1'] = ['비밀번호가 일치하지 않습니다.']
            errors['password2'] = ['비밀번호가 일치하지 않습니다.']
        else:
            password_error = validate_password(password1, password2)
            errors.update(password_error)
        if not is_privacy_policy:
            errors['is_privacy_policy'] = ['개인정보취급방침에 동의하셔야 합니다.']
        if not is_terms_of_service:
            errors['is_terms_of_service'] = ['서비스이용약관에 동의하셔야 합니다.']
        if errors:
            raise ValidationError(errors)

        return attrs

    @transaction.atomic
    def create(self, validated_data):
        validated_data.pop('password2')
        validated_data['password'] = validated_data.pop('password1')
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
            email=validated_data['email'],
            is_privacy_policy=validated_data['is_privacy_policy'],
            is_terms_of_service=validated_data['is_terms_of_service'],
            is_active=False,
        )
        celeb = Celeb.objects.create(
            user=user,
            name=validated_data['name'],
            phone=validated_data['phone'],
        )
        for sns_data in validated_data['sns_set']:
            SNS.objects.create(celeb=celeb, **sns_data)
        return celeb


class CelebProductListSerializer(serializers.ModelSerializer):
    seller_name = serializers.CharField(source='seller.name')

    class Meta:
        model = Product
        fields = ['id', 'title', 'price', 'seller_name', 'main_thumbnail']


class ThumbnailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thumbnail
        fields = ['id', 'thumbnail_url', 'order']


class OptionValueSerializer(serializers.ModelSerializer):
    option_name = serializers.CharField(source='option.name')

    class Meta:
        model = OptionValue
        fields = ['id', 'option_name', 'name', 'price']


class OptionSerializer(serializers.ModelSerializer):
    option_value_set = OptionValueSerializer(many=True)

    class Meta:
        model = Option
        fields = ['id', 'name', 'order', 'option_value_set']


class OptionCombinationSerializer(serializers.ModelSerializer):
    option_value_set = OptionValueSerializer(many=True)

    class Meta:
        model = OptionCombination
        fields = ['id', 'stock', 'option_value_set']


class CelebProductDetailSerializer(serializers.ModelSerializer):
    thumbnail_set = ThumbnailSerializer(many=True)
    seller_name = serializers.CharField(source='seller.name')
    celeb_id = serializers.SerializerMethodField()
    option_set = OptionSerializer(many=True)
    option_combination_set = OptionCombinationSerializer(many=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'seller_name',
            'celeb_id',
            'main_thumbnail',
            'thumbnail_set',
            'title',
            'price',
            'is_stock',
            'option_set',
            'option_combination_set',
        ]

    def get_celeb_id(self, obj):
        request = self.context['request']
        celeb_name = request.parser_context['kwargs']['celeb_name']
        return obj.celeb_set.get(name=celeb_name).id


class CelebCelebProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = CelebProduct
        fields = ['product']

    def validate(self, attrs):
        product = attrs['product']
        request = self.context['request']
        celeb = request.user.celeb
        try:
            celeb.celebproduct_set.get(product=product)
            raise ValidationError({'product': ['이미 판매중인 상품입니다.']})
        except CelebProduct.DoesNotExist:
            pass
        return attrs

    def create(self, validated_data):
        product = validated_data['product']
        request = self.context['request']
        celeb = request.user.celeb
        count = celeb.celebproduct_set.count()
        celeb_product = CelebProduct.objects.create(celeb=celeb, product=product, ordering=count)
        return celeb_product


class CelebProductOrderingSerializer(serializers.Serializer):
    product_ordering = serializers.DictField(child=serializers.IntegerField(), write_only=True)

    @transaction.atomic
    def update(self, instance, validated_data):
        product_ordering = validated_data['product_ordering']
        for product_id, ordering in product_ordering.items():
            celebproduct = instance.celebproduct_set.get(product_id=product_id)
            celebproduct.ordering = ordering
            celebproduct.save()

        return {}
