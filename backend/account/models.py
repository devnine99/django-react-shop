from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser, PermissionsMixin, UserManager
from django.db import models


class User(AbstractUser):
    first_name = False
    last_name = False
    username = models.CharField(verbose_name='유저네임', max_length=16, unique=True)
    email = models.EmailField(verbose_name='이메일', blank=True)
    is_email = models.BooleanField(verbose_name='이메일 검증', default=False)
    password = models.CharField(verbose_name='비밀번호', max_length=128)

    is_privacy_policy = models.BooleanField(verbose_name='개인정보 취급방침 동의', default=False)
    is_terms_of_service = models.BooleanField(verbose_name='서비스 이용약관 동의', default=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'

    class Meta:
        verbose_name = '유저'
        verbose_name_plural = verbose_name

    def get_type(self):
        if hasattr(self, 'celeb'):
            return 'celeb'
        elif hasattr(self, 'seller'):
            return 'seller'
        else:
            return 'customer'

