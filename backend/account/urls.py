from django.urls import path

from account.views import LoginView, LogoutView, RegisterView, ProfileView, PasswordResetView, PasswordResetConfirmView, \
    CelebFollowView, EmailVerificationView, EmailVerificationConfirmView

app_name = 'account'
urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('email/verification/', EmailVerificationView.as_view(), name='email_verification'),
    path('email/verification/confirm/', EmailVerificationConfirmView.as_view(), name='email_verification_confirm'),
    path('password/reset/', PasswordResetView.as_view(), name='password_reset'),
    path('password/reset/confirm/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('<celeb_id>/follow/', CelebFollowView.as_view()),
]
