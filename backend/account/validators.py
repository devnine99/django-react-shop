import re

from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import UserAttributeSimilarityValidator, CommonPasswordValidator, \
    NumericPasswordValidator

User = get_user_model()


def get_username_validators():
    validators = [
        ASCIIUsernameValidator(),
        CustomLengthValidator(4, 16),
    ]
    return validators


def validate_username(username):
    validators = get_username_validators()
    error = {}
    username_error = []
    for validator in validators:
        msg = validator.validate(username)
        if msg:
            username_error.append(msg)
    if username_error:
        error['username'] = username_error
    return error


def get_password_validators():
    validators = [
        CustomLengthValidator(8, 16),
        CustomUserAttributeSimilarityValidator(),
        CustomCommonPasswordValidator(),
        CustomNumericPasswordValidator(),
    ]
    return validators


def validate_password(password1, password2):
    validators = get_password_validators()
    error = {}
    password1_error = []
    password2_error = []
    if password1 == password2:
        for validator in validators:
            msg = validator.validate(password1)
            if msg:
                password1_error.append(msg)
                password2_error.append(msg)
    if password1_error:
        error['password1'] = password1_error
    if password2_error:
        error['password2'] = password2_error
    return error


class ASCIIUsernameValidator:
    def __init__(self):
        regex = r'^[\w]+\Z'
        self.p = re.compile(regex, re.ASCII)

    def validate(self, username):
        if not self.p.match(username):
            msg = '영문, 숫자, 밑줄만 가능합니다.'
            return msg


class CustomLengthValidator:
    def __init__(self, min_length, max_length):
        self.min_length = min_length
        self.max_length = max_length

    def validate(self, fields, user=None):
        if len(fields) < self.min_length:
            msg = f'최소 {self.min_length} 문자를 입력해주세요.'
            return msg
        if len(fields) > self.max_length:
            msg = f'최대 {self.max_length} 문자를 입력해주세요.'
            return msg


class CustomUserAttributeSimilarityValidator(UserAttributeSimilarityValidator):
    def validate(self, password, user=None):
        if not user:
            return


class CustomCommonPasswordValidator(CommonPasswordValidator):
    def validate(self, password, user=None):
        if password.lower().strip() in self.passwords:
            msg = '비밀번호가 공통적인 글자입니다.'
            return msg


class CustomNumericPasswordValidator(NumericPasswordValidator):
    def validate(self, password, user=None):
        if password.isdigit():
            msg = '비밀번호가 숫자로만 되어있습니다.'
            return msg
