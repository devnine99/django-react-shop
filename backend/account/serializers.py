from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError

import settings
from account.tokens import email_verification_token
from account.validators import validate_password, validate_username

User = get_user_model()


class LoginSerializer(serializers.Serializer):
    token = serializers.CharField(source='auth_token', read_only=True)
    username = serializers.CharField()
    password = serializers.CharField(write_only=True)
    type = serializers.CharField(source='get_type', read_only=True)

    def validate(self, attrs):
        username = attrs['username']
        password = attrs['password']

        errors = dict()
        try:
            user = User.objects.get(username=username)
            if not user.is_active:
                errors['username'] = ['활성화되지 않은 사용자입니다.', '가입 승인 후 이용가능합니다.']
            if not check_password(password, user.password):
                errors['password'] = ['비밀번호가 틀립니다.']
        except User.DoesNotExist:
            errors['username'] = ['존재하지 않는 유저입니다.']
        if errors:
            raise ValidationError(errors)

        return attrs

    def create(self, validated_data):
        instance = authenticate(**validated_data)
        Token.objects.get_or_create(user=instance)

        return instance


class LogoutSerializer(serializers.Serializer):
    def create(self, validated_data):
        request = self.context['request']
        instance = request.user
        instance.auth_token.delete()

        return instance


class RegisterSerializer(serializers.Serializer):
    token = serializers.CharField(source='auth_token', read_only=True)
    username = serializers.CharField()
    email = serializers.EmailField(write_only=True)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    is_privacy_policy = serializers.BooleanField(write_only=True)
    is_terms_of_service = serializers.BooleanField(write_only=True)
    type = serializers.CharField(source='get_type', read_only=True)

    def validate(self, attrs):
        username = attrs['username']
        email = attrs['email']
        password1 = attrs['password1']
        password2 = attrs['password2']
        is_privacy_policy = attrs['is_privacy_policy']
        is_terms_of_service = attrs['is_terms_of_service']

        errors = dict()
        if User.objects.filter(username=username).exists():
            errors['username'] = ['이미 존재하는 유저입니다.']
        else:
            username_error = validate_username(username)
            errors.update(username_error)
        if password1 != password2:
            errors['password1'] = ['비밀번호가 일치하지 않습니다.']
            errors['password2'] = ['비밀번호가 일치하지 않습니다.']
        else:
            password_error = validate_password(password1, password2)
            errors.update(password_error)
        if not is_privacy_policy:
            errors['is_privacy_policy'] = ['개인정보취급방침에 동의하셔야 합니다.']
        if not is_terms_of_service:
            errors['is_terms_of_service'] = ['서비스이용약관에 동의하셔야 합니다.']
        if errors:
            raise ValidationError(errors)

        return attrs

    def create(self, validated_data):
        validated_data.pop('password2')
        validated_data['password'] = validated_data.pop('password1')
        instance = User.objects.create_user(**validated_data)
        Token.objects.get_or_create(user=instance)

        return instance


class ProfileSerializer(serializers.Serializer):
    username = serializers.CharField(read_only=True)
    email = serializers.EmailField()
    is_email = serializers.BooleanField(read_only=True)
    password1 = serializers.CharField(write_only=True, required=False, allow_blank=True)
    password2 = serializers.CharField(write_only=True, required=False, allow_blank=True)

    def send_email_verification_email(self, user, email):
        request = self.context['request']
        current_site = get_current_site(request)
        subject = f'{current_site.name} 이메일 확인'
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': email_verification_token.make_token(user),
            'protocol': 'https' if request.is_secure() else 'http',
        }
        html_email = loader.render_to_string('account/email_verification_email.html', context)
        from_email = getattr(settings, 'DEFAULT_FROM_EMAIL')
        to = [email]

        email_message = EmailMultiAlternatives(
            subject=subject,
            from_email=from_email,
            to=to,
            alternatives=[(html_email, 'text/html')]
        )
        email_message.send()

    def validate(self, attrs):
        # username = attrs['username']
        email = attrs['email']
        password1 = attrs.get('password1')
        password2 = attrs.get('password2')

        errors = dict()
        # if username != self.instance.username and User.objects.filter(username=username).exists():
        #     errors['username'] = ['이미 존재하는 유저입니다.']
        # else:
        #     username_error = validate_username(username)
        #     errors.update(username_error)
        if password1 and password2:
            if password1 != password2:
                errors['password1'] = ['비밀번호가 일치하지 않습니다.']
                errors['password2'] = ['비밀번호가 일치하지 않습니다.']
            else:
                password_error = validate_password(password1, password2)
                errors.update(password_error)
        if errors:
            raise ValidationError(errors)

        return attrs

    def update(self, instance, validated_data):
        # username = validated_data['username']
        email = validated_data['email']
        password = validated_data.get('password1')
        # user.username = username
        if not email == instance.email:
            instance.email = email
            instance.is_email = False
            self.send_email_verification_email(instance, email)
        else:
            instance.email = email
        if password:
            password = make_password(password)
            instance.password = password
        instance.save()

        return instance


class PasswordChangeSerializer(serializers.Serializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    def validate(self, attrs):
        password1 = attrs['password1']
        password2 = attrs['password2']

        errors = dict()
        if password1 != password2:
            errors['password1'] = ['비밀번호가 일치하지 않습니다.']
            errors['password2'] = ['비밀번호가 일치하지 않습니다.']
        else:
            password_error = validate_password(password1, password2)
            errors.update(password_error)
        if errors:
            raise ValidationError(errors)

        return attrs

    def update(self, instance, validated_data):
        password = validated_data['password1']
        instance.set_password(password)
        instance.save()
        instance.auth_token.delete()

        return instance


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate(self, attrs):
        email = attrs['email']

        errors = dict()
        if not User.objects.filter(email=email).exists():
            errors['email'] = ['가입된 이메일이 없습니다.']
        if errors:
            raise ValidationError(errors)

        return attrs

    def send_password_reset_email(self, user):
        request = self.context['request']
        current_site = get_current_site(request)
        subject = f'{current_site.name} 비밀번호 재설정'
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'https' if request.is_secure() else 'http',
        }
        html_email = loader.render_to_string('account/password_reset_email.html', context)
        from_email = getattr(settings, 'DEFAULT_FROM_EMAIL')
        to = [user.email]

        email_message = EmailMultiAlternatives(
            subject=subject,
            from_email=from_email,
            to=to,
            alternatives=[(html_email, 'text/html')]
        )
        email_message.send()

    def create(self, validated_data):
        instance = User.objects.get(**validated_data)
        self.send_password_reset_email(instance)

        return instance


class PasswordResetConfirmSerializer(serializers.Serializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    uid = serializers.CharField(write_only=True)
    token = serializers.CharField(write_only=True)

    def validate(self, attrs):
        password1 = attrs['password1']
        password2 = attrs['password2']
        token = attrs['token']
        user = self.instance

        if not user.is_authenticated:
            raise ValidationError({'uid': '존재하지 않는 유저입니다.'})

        if not default_token_generator.check_token(user, token):
            raise ValidationError({'token': '이미 비밀번호를 변경하셨습니다.'})

        errors = dict()
        if password1 != password2:
            errors['password1'] = ['비밀번호가 일치하지 않습니다.']
            errors['password2'] = ['비밀번호가 일치하지 않습니다.']
        else:
            password_error = validate_password(password1, password2)
            errors.update(password_error)
        if errors:
            raise ValidationError(errors)

        return attrs

    def update(self, instance, validated_data):
        password = validated_data['password1']
        instance.set_password(password)
        instance.save()

        return instance


class EmailVerificationSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def send_email_verification_email(self, user):
        request = self.context['request']
        current_site = get_current_site(request)
        subject = f'{current_site.name} 이메일 확인'
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': email_verification_token.make_token(user),
            'protocol': 'https' if request.is_secure() else 'http',
        }
        html_email = loader.render_to_string('account/email_verification_email.html', context)
        from_email = getattr(settings, 'DEFAULT_FROM_EMAIL')
        to = [user.email]

        email_message = EmailMultiAlternatives(
            subject=subject,
            from_email=from_email,
            to=to,
            alternatives=[(html_email, 'text/html')]
        )
        email_message.send()

    def update(self, instance, validated_data):
        email = validated_data['email']
        instance.email = email
        instance.is_email = False
        self.send_email_verification_email(instance)
        instance.save()

        return instance


class EmailVerificationConfirmSerializer(serializers.Serializer):
    email = serializers.EmailField(read_only=True)
    uid = serializers.CharField(write_only=True)
    token = serializers.CharField(write_only=True)

    def validate(self, attrs):
        token = attrs['token']
        user = self.instance

        if not user.is_authenticated:
            raise ValidationError({'uid': '존재하지 않는 유저입니다.'})
        if not email_verification_token.check_token(user, token):
            raise ValidationError({'token': '이미 이메일 확인을 완료하셨습니다.'})

        errors = dict()
        if errors:
            raise ValidationError(errors)

        return attrs

    def update(self, instance, validated_data):
        instance.is_email = True
        instance.save()

        return instance


class CelebFollowSerializer(serializers.Serializer):
    followed = serializers.BooleanField()

    def update(self, instance, validated_data):
        request = self.context['request']
        celeb_id = request.parser_context['kwargs']['celeb_id']
        if instance.following.filter(id=celeb_id).exists():
            instance.following.remove(celeb_id)
            followed = False
        else:
            instance.following.add(celeb_id)
            followed = True
        return {'followed': followed}
