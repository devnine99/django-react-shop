from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework.generics import UpdateAPIView, CreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated

from account.serializers import LoginSerializer, RegisterSerializer, ProfileSerializer, PasswordResetSerializer, \
    PasswordResetConfirmSerializer, CelebFollowSerializer, EmailVerificationSerializer, \
    EmailVerificationConfirmSerializer, LogoutSerializer, PasswordChangeSerializer

User = get_user_model()


class LoginView(CreateAPIView):
    serializer_class = LoginSerializer


class LogoutView(CreateAPIView):
    serializer_class = LogoutSerializer
    permission_classes = [IsAuthenticated]


class RegisterView(CreateAPIView):
    serializer_class = RegisterSerializer


class ProfileView(RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user


class PasswordChangeView(UpdateAPIView):
    serializer_class = PasswordChangeSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user


class PasswordResetView(CreateAPIView):
    serializer_class = PasswordResetSerializer


class PasswordResetConfirmView(UpdateAPIView):
    serializer_class = PasswordResetConfirmSerializer

    def get_object(self):
        uid = self.request.data.get('uid')
        uid = force_text(urlsafe_base64_decode(uid))
        try:
            return User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            return AnonymousUser()


class EmailVerificationView(UpdateAPIView):
    serializer_class = EmailVerificationSerializer
    permission_classes = [IsAuthenticated]

    def get_object(self):
        return self.request.user


class EmailVerificationConfirmView(UpdateAPIView):
    serializer_class = EmailVerificationConfirmSerializer

    def get_object(self):
        uid = self.request.data.get('uid')
        uid = force_text(urlsafe_base64_decode(uid))
        try:
            return User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            return AnonymousUser()


class CelebFollowView(UpdateAPIView):
    serializer_class = CelebFollowSerializer

    def get_object(self):
        return self.request.user
